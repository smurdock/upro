/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Test;



import com.ihcfs.UniverseEntities.conversion.ETLService;

import junit.framework.*;


public class TestPaymentHistory extends TestCase {

public TestPaymentHistory(String name) {
	super(name);
}

public void testReadWrite() throws Exception
{

	
	ETLService payplanService = new ETLService();
	payplanService.setListName("PAYMENT-HISTORY");
	payplanService.setFileName("PAYMENT-HISTORY");
	payplanService.setLoadMax(2001);
	
	Thread thread11 = new Thread(payplanService);
	
	thread11.start();
	
	while (!payplanService.isFinished())
	{
		Thread.sleep(2000);
		System.out.println("Loaded "+payplanService.getLoadCount()+" "+payplanService.getFileName()+" records");

	}



}
}

package com.ihcfs.UniverseEntities.Test;

import com.ihcfs.UniverseEntities.Data.ClientDO;
import com.ihcfs.UniverseEntities.Data.DebtorDO;
import com.ihcfs.UniverseEntities.EntityManager.UniverseEntityManager;

import java.util.List;

/**
 * Created by Administrator on 1/13/2017.
 */
public class TestEntityManager {

    public static void main(String[] args) throws Exception{
        UniverseEntityManager entityManager = new UniverseEntityManager("smurdock","Fr@udpr00f", UniverseEntityManager.SESSION_TYPE_UNIOBJECTS);
        //List<DebtorDO> debtorRecords = entityManager.find(DebtorDO.class, DebtorDO.class, "7202818");
        List<ClientDO> clientRecords = entityManager.find(ClientDO.class, ClientDO.class,"QB2NYJ");
        entityManager.close();
        System.out.println(clientRecords.get(0).getClientName());
        //System.out.println(debtorRecords.get(0).getDebtorName());
    }
}

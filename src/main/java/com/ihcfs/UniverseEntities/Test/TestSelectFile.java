package com.ihcfs.UniverseEntities.Test;

import asjava.uniclientlibs.UniString;
import asjava.uniobjects.UniSelectList;
import com.ihcfs.UniverseEntities.EntityManager.UniverseEntityManager;

/**
 * Created by Administrator on 5/26/2016.
 */
public class TestSelectFile {

    public static void main (String[] args) throws Exception
    {
        UniverseEntityManager entityManager = new UniverseEntityManager();
        entityManager.selectFile("DEBTOR");
        UniSelectList selectList=entityManager.getSelect("SEAN");

        boolean done = false;
        while(!done) {
            UniString uniString=selectList.next();
            if (uniString==null){
                done=true;
            } else{
                System.out.println(uniString.toString());
            }
        }

        entityManager.closeSelect("SEAN");
        entityManager.close();

    }

}

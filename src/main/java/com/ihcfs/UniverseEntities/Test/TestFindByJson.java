package com.ihcfs.UniverseEntities.Test;

import com.ihcfs.UniverseEntities.Data.DebtorDO;
import com.ihcfs.UniverseEntities.EntityManager.UniverseEntityManager;

import java.util.List;

/**
 * Created by Administrator on 12/12/2016.
 */
public class TestFindByJson {

    public static void main (String[] args) throws Exception{
        UniverseEntityManager entityManager = new UniverseEntityManager("Administrator","",UniverseEntityManager.SESSION_TYPE_UNIOBJECTS);
        List<DebtorDO> debtors =entityManager.findFromJSON(DebtorDO.class, DebtorDO.class, "100");

        for (DebtorDO debtor: debtors){
            System.out.println(debtor.getDebtorName());
        }
    }
}

package com.ihcfs.UniverseEntities.Test;

import com.ihcfs.UniverseEntities.PickUtilities.TimeDateConverter;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 4/21/2017.
 */
public class TestTimeDateConverter {

    public static void main (String[] args) throws Exception{

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String testString="2017/11/21 09:00:00";

        Date testDate=dateFormat.parse(testString);
        Long internalDate=TimeDateConverter.internalDate(testDate);

        System.out.println("Parsed date: "+testString+" as internal date: "+internalDate);

        testString= "2017/11/22 00:00:00";
        testDate = dateFormat.parse(testString);
        internalDate = TimeDateConverter.internalDate(testDate);

        System.out.println("Parsed date: "+testString+" as internal date: "+internalDate);

        String pickEpochString = "1967/12/31 00:00:00";
        Date pickEpochDate = dateFormat.parse(pickEpochString);

        internalDate = TimeDateConverter.internalDate(pickEpochDate);
        System.out.println("Parsed date: "+pickEpochString+" as internal date: "+internalDate);

        String oneInTheMorningNewYearsEve = "1967/12/31 01:00:00";
        Date oneInTheMorningDate = dateFormat.parse(oneInTheMorningNewYearsEve);

        internalDate = TimeDateConverter.internalDate(oneInTheMorningDate);
        System.out.println("Parsed date: "+oneInTheMorningNewYearsEve+" as internal date: "+internalDate);

        Date javaEpochDate = new Date(0l);

        System.out.println("Java epoch date: 0 as external date: "+dateFormat.format(javaEpochDate));


        Long pickEpochLong = pickEpochDate.getTime();

        System.out.println("Pick epoch date: "+pickEpochString);

        System.out.println("Pick epoch internal java time: "+pickEpochLong);

        Long daysBetweenJavaEpochAndPickEpoch = pickEpochLong/(24l*60l*60l*1000l);

        System.out.println("Days between pick epoch date and java epoch date: "+daysBetweenJavaEpochAndPickEpoch);

    }
}

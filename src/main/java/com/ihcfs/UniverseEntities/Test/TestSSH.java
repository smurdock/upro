package com.ihcfs.UniverseEntities.Test;

import com.ihcfs.UniverseEntities.EntityManager.UniverseUserInfo;
import com.ihcfs.UniverseEntities.config.ConfigurationUtility;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.InputStream;

/**
 * Created by Administrator on 1/4/2017.
 */
public class TestSSH {
    private static Session remoteShellSession;
    private static JSch jsch=new JSch();
    public static void main(String[] args) throws Exception{


        UniverseUserInfo userInfo = new UniverseUserInfo();
        userInfo.setPassword("LKan3200");
        remoteShellSession=jsch.getSession("seanm", "35.163.212.45",22 );
        remoteShellSession.setUserInfo(userInfo);
        remoteShellSession.connect();
        String command = "cd /tmp; ls";
        System.out.println("Executing command: "+command);
        String response = executeShellCommand(command);
        System.out.println("Response: "+response);
        String command2 = "ls";
        System.out.println("Executing command: "+command2);
        String response2 = executeShellCommand(command2);
        System.out.println("Response: "+response2);

//        Channel channel = remoteShellSession.openChannel("exec");
//        ((ChannelExec) channel).setCommand("pwd");
//        channel.setInputStream(null);
//        ((ChannelExec)channel).setErrStream(System.err);
//        InputStream in = channel.getInputStream();
//        channel.connect();
//
//        byte[] tmp = new byte[1024];
//        while(true){
//            while(in.available()>0){
//                int i=in.read(tmp,0,1024);
//                if(i<0) break;
//                System.out.println(new String(tmp, 0, i));
//            }
//            if (channel.isClosed()){
//                if (in.available()>0) continue;
//                System.out.println("exit-status: "+channel.getExitStatus());
//                break;
//            }
//            try{Thread.sleep(1000);}catch(Exception ee){}
//        }
//
//        channel.disconnect();
        remoteShellSession.disconnect();
    }

    private static String executeShellCommand(String command) throws Exception{
        Channel channel = remoteShellSession.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);
        channel.setInputStream(null);
        ((ChannelExec)channel).setErrStream(System.err);
        InputStream in = channel.getInputStream();
        channel.connect();

        String response = "";
        byte[] tmp = new byte[1024];
        boolean hasMore=true;
        while(hasMore){
            while(in.available()>0 && hasMore){
                int i=in.read(tmp,0,1024);
                if(i<0) hasMore=false;
                response+=new String(tmp, 0, i);
            }
            if (channel.isClosed()){
                if (in.available()>0) continue;
                System.out.println("exit-status: "+channel.getExitStatus());
                break;
            }
            try{Thread.sleep(1000);}catch(Exception ee){}
        }

        channel.disconnect();
        return response;

    }
}

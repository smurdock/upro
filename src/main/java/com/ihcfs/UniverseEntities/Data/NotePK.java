/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;


@Embeddable
public class NotePK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8683371085032235788L;

	@UniverseColumn(name="0")
	@Column(name="ACCOUNTID")
	private Integer accountId;
	
	@Column(name="SEQUENCE")
	private Integer sequence;
	
	public Integer getAccountId(){
		return accountId;
	}
	
	public void setAccountId(Integer accountId){
		this.accountId=accountId;
	}
	
	public Integer getSequence(){
		return sequence;
	}
	
	public void setSequence(Integer sequence){
		this.sequence=sequence;
	}

}

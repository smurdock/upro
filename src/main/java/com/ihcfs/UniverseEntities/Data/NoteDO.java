/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.ihcfs.UniverseEntities.PickUtilities.TimeDateConverter;

/**
 * Entity implementation class for Entity: NoteDO
 *
 */
@Entity(name="PHPMMEMO")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"accountId","sequence"}))
@UniverseExtractionType(name="MEMO")

public class NoteDO implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public NoteDO(){
		
	}
	
	public String toString(){
		String actionCodePadded="";
		if (actionCode!=null && !"".equals(actionCode))
		{
			actionCodePadded=" "+actionCode;
		}
		
		String noteLinePadded="";
		if (noteLine!=null && !"".equals(noteLine))
		{
			noteLinePadded=" "+noteLine;
		}
		
		String agentInitialsPadded="";
		if (agentInitials!=null && !"".equals(agentInitials))
		{
			agentInitialsPadded=" "+agentInitials;
		}
		return String.valueOf(TimeDateConverter.internalDate(noteDate))+" "+String.valueOf(TimeDateConverter.internalTime(noteDate))+actionCodePadded;
	}
	
	public NoteDO(Integer accountId, Integer sequence, String noteLine) {
		super();
		notePK = new NotePK();
		notePK.setAccountId(accountId);
		notePK.setSequence(sequence);
		this.noteLine=noteLine;
	}
	
	private Date noteDate;
	private String noteLine;
	private Integer actionCode;
	private String agentInitials;
	private Long jobId;
	private Boolean systemNote;
	
	@EmbeddedId
	protected NotePK notePK;

	public String getAgentInitials(){
		return agentInitials;
	}
	
	public void setAgentInitials(String agentInitials){
		this.agentInitials=agentInitials;
	}
	
	public Long getJobId(){
		return jobId;
	}

	public void setJobId(Long jobId){
		this.jobId=jobId;
	}
	
	public Date getNoteDate() {
		return noteDate;
	}
	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}
	public String getNoteLine() {
		return noteLine;
	}
	public void setNoteLine(String noteLine) {
		this.noteLine = noteLine;
	}
	public Integer getActionCode() {
		return actionCode;
	}
	public void setActionCode(Integer actionCode) {
		this.actionCode = actionCode;
	}
	
	public NotePK getNotePK(){
		return notePK;
	}
	
	public Boolean getSystemNote(){
		return systemNote;
	}
	
	public void setSystemNote(Boolean systemNote){
		this.systemNote=systemNote;
	}
   
}

/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.ihcfs.UniverseEntities.EntityManager.EMFactory;

public class GenericDO extends EMFactory {
	
	public void persistObject(Object object) throws Exception
	{
		EntityManager em = createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(object);
		tx.commit();
		em.close();
			
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getAllObjects() throws Exception
	{
		EntityManager em = createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		Query allObjectsQuery = em.createQuery("select o from "+Object.class.getName()+" d");
		
		List<Object> allObjects = allObjectsQuery.getResultList();
		
		tx.commit();
		em.close();
		return allObjects;
	}

}

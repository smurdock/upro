/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 

package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;

@Entity(name="PHPMCLIENT")
@UniverseTable(name="CLIENT")

public class ClientDO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 331528933295075978L;

	@UniverseColumn(name="0")
	@Id
	private String clientId;
	
	@UniverseColumn(name="1.1")
	private String clientName;

	@UniverseColumn(name="10")
	private String businessClass;
	
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setBusinessClass(String businessClass) {
		this.businessClass = businessClass;
	}

	public String getBusinessClass() {
		return businessClass;
	}

	
	
}

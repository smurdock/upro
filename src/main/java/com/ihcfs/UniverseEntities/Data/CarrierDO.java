/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;
import jakarta.xml.bind.annotation.XmlRootElement;

@Entity(name="PHPMCARRIER")
@UniverseTable(name="CARRIER")

@XmlRootElement(name="carrier")
public class CarrierDO implements Serializable {

	private static final long serialVersionUID = 7257535681078124996L;

	@UniverseColumn(name="0")
	@Id
	private String carrierId;
	
	@UniverseColumn(name="1")
	private String carrierName;

	private Long jobId;
	
	public CarrierDO() {
		super();
	}

	
	public String getCarrierId() {
		return carrierId;
	}


	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}


	public String getCarrierName() {
		return carrierName;
	}


	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}

}

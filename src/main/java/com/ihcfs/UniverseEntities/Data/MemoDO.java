/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.util.ArrayList;

import javax.persistence.Id;

@UniverseTable(name="MEMO")
public class MemoDO  {

	@UniverseColumn(name="0")
	@Id
	private Integer accountId;	
	private Long sequence;
	@UniverseColumn(name="1")
	private Long memoCount;
	@UniverseColumn(name="note")
	@Repeating(name="note",firstAttribute="2")	
	private NoteDO note;
	
	public Integer getAccountId(){
		return accountId;
	}
	
	public void setAccountId(Integer accountId){
		this.accountId=accountId;
	}
	
	public Long getMemoCount(){
		return memoCount;
	}
	
	public void setMemoCount(Long memoCount){
		this.memoCount=memoCount;
	}
	
	public NoteDO getNote(){
		return note;
	}
	
	public void setNote(NoteDO note){
		this.note=note;
	}
	
	public Long getSequence(){
		return sequence;
	}
	
	public void setSequence(Long sequence){
		this.sequence=sequence;
	}
	
}

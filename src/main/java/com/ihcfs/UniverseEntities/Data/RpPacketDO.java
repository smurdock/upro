/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;

@Entity(name="PHPMRPPACKET")
@UniverseTable(name="RP-PACKET")

public class RpPacketDO implements Serializable {

	
	private static final long serialVersionUID = 2337174918286341252L;

	
	@EmbeddedId
	protected RpPacketPK rpPacketPK;
	
	@UniverseColumn(name="1")
	@MultiValuedColumn
	private Long accountId;
	private Long jobId;
	
	
	public RpPacketDO() {
		super();
	}
	
	public RpPacketPK getRPPacketPK()
	{
		return rpPacketPK;
	}
	

	public Long getAccountId() {
		return accountId;
	}


	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}


}

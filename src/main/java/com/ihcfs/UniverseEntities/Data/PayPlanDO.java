/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: PayPlanDO
 *
 */
@Entity(name="PHPMPAYPLAN")
@UniverseTable(name="PAYPLAN")

public class PayPlanDO implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7754571364807450262L;


	@UniverseColumn(name="1")
	@MultiValuedColumn
	private Integer accountId;	
	
	@EmbeddedId
	protected PayPlanPK payPlanPK;
	
	@UniverseColumn(name="4")
	@MultiValuedColumn
	private Date addedToPlanDate;
	
	@UniverseColumn(name="41")
	@MultiValuedColumn
	private BigDecimal interestBearingPrincipalAmount;
	
	private Long jobId;
	
	@UniverseColumn(name="42")
	@MultiValuedColumn
	private BigDecimal nonInterestBearingPrincipalAmount;	
	
	public PayPlanDO() {
		super();
	}
	
	public PayPlanPK getPayPlanPK()
	{
		return payPlanPK;
	}
	
	public Integer getAccountId(){
		return accountId;
	}
	
	public void setAccountId(Integer accountId)
	{
		this.accountId=accountId;
	}
	
	public Date getAddedToPlanDate() {
		return addedToPlanDate;
	}

	public void setAddedToPlanDate(Date addedToPlanDate) {
		this.addedToPlanDate = addedToPlanDate;
	}

	public BigDecimal getInterestBearingPrincipalAmount() {
		return interestBearingPrincipalAmount;
	}

	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}
	
	public void setInterestBearingPrincipalAmount(
			BigDecimal interestBearingPrincipalAmount) {
		this.interestBearingPrincipalAmount = interestBearingPrincipalAmount;
	}

	public BigDecimal getNonInterestBearingPrincipalAmount() {
		return nonInterestBearingPrincipalAmount;
	}

	public void setNonInterestBearingPrincipalAmount(
			BigDecimal nonInterestBearingPrincipalAmount) {
		this.nonInterestBearingPrincipalAmount = nonInterestBearingPrincipalAmount;
	}

   
}

/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;

@Entity(name="PHPMTRANSCODEFILE")
@UniverseTable(name="TRANS-CODE-FILE")

public class TransCodeFileDO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5468438418835700820L;

	@UniverseColumn(name="0")
	@Transient
	private Integer transCodeFileId;
	
	@UniverseColumn(name="1.6")
	@Transient
	private String forward;
	
	@UniverseColumn(name="1.12")
	@Transient
	private String transType;

	@Transient
	private String transTypeDescription;

	@Column(name = "TRANSCODEFILEID")
	@Id
	public Integer getTransCodeFileId() {
		return transCodeFileId;
	}

	public void setTransCodeFileId(Integer transCodeFileId) {
		this.transCodeFileId = transCodeFileId;
	}

	@Column(name = "FORWARD")
	public String getForward() {
		return forward;
	}
	
	public void setForward(String forward) {
		this.forward = forward;
	}

	@Column(name = "TRANSTYPE")
	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}
	
	public void setTransTypeDescription(String transTypeDescription) {
		this.transTypeDescription = transTypeDescription;
	}

	@Column(name = "TRANSTYPEDESCRIPTION")
	public String getTransTypeDescription() {
		transTypeDescription="nothing";
		char transType1 = ' ';
		if (transType != null && transType.trim().length() > 0)
			transType1 = transType.trim().charAt(0);
		
		switch (transType1){
			
		case 'A': transTypeDescription="Attorney";
		        break;
		
		case 'C': transTypeDescription="Court Costs";
				break;
		
		case 'I': transTypeDescription="Interest";
				break;
				
		case 'M': transTypeDescription="Miscellaneous";
				break;
				
		case 'N': transTypeDescription="Overpayment";
				break;
		
		case 'P': transTypeDescription="Principal";
				break;
		
		}
		
		return transTypeDescription;
	}
	
}

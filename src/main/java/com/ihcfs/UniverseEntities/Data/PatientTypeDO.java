/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import javax.persistence.*;

@Entity(name="PHPMPATIENTTYPE")
@UniverseTable(name="PATIENT.TYPE")

public class PatientTypeDO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4433150226369346186L;
	
	@UniverseColumn(name="0")
	@Id
	private String patientTypeId;
	
	@UniverseColumn(name="1")
	private String patientTypeDescription;

	public String getPatientTypeId() {
		return patientTypeId;
	}

	public void setPatientTypeId(String patientTypeId) {
		this.patientTypeId = patientTypeId;
	}

	public String getPatientTypeDescription() {
		return patientTypeDescription;
	}

	public void setPatientTypeDescription(String patientTypeDescription) {
		this.patientTypeDescription = patientTypeDescription;
	}
	

}

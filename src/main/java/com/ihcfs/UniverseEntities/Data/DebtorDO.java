/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import javax.persistence.*;
import jakarta.xml.bind.annotation.XmlRootElement;

@Entity(name="PHPMDEBTOR")
@UniverseTable(name="DEBTOR")

@XmlRootElement(name="account")
public class DebtorDO implements Serializable{
	
	/**
	 * 
	 */
	public static final long serialVersionUID = 6463008215354372324L;

	@UniverseColumn(name="0")
	@Id
	private Integer debtorNumber;

	@UniverseColumn(name="76.4")
	private Date accollaidReceiveDate;	
	
	@UniverseColumn(name="76.3")
	private Date accollaidRequestDate;	
	
	@UniverseColumn(name="63.3")
	private BigDecimal assignFeesCommRate;	

	@UniverseColumn(name="63.2")
	private BigDecimal assignInterestCommRate;	
	
	@UniverseColumn(name="63.4")
	private BigDecimal assignMisc1CommRate;	
	
	@UniverseColumn(name="63.5")
	private BigDecimal assignMisc2CommRate;	
	
	@UniverseColumn(name="63.6")
	private BigDecimal assignMisc3CommRate;	

	@UniverseColumn(name="63.7")
	private BigDecimal assignMisc4CommRate;	
	
	@UniverseColumn(name="63.8")
	private BigDecimal assignMisc5CommRate;
	
	@UniverseColumn(name="63.9")
	private BigDecimal assignMisc6CommRate;	
	
	@UniverseColumn(name="63.10")
	private BigDecimal assignMisc7CommRate;	
	
	@UniverseColumn(name="63.11")
	private BigDecimal assignMisc8CommRate;	
	
	@UniverseColumn(name="63.12")
	private BigDecimal assignMisc9CommRate;	
	
	@UniverseColumn(name="63.1")
	private BigDecimal assignPrincipalCommRate;	
	
	@UniverseColumn(name="15")
	private BigDecimal assignedAmountOwing;	
	
	@UniverseColumn(name="16")
	private BigDecimal assignedAmountReceived;
	
	@UniverseColumn(name="60.3")
	private BigDecimal assignedCommFeesOwing;	
	
	@UniverseColumn(name="61.3")
	private BigDecimal assignedCommFeesReceived;	
	
	@UniverseColumn(name="14")
	private Date assignedDate;
	
	@UniverseColumn(name="60.2")
	private BigDecimal assignedInterestOwing;	
	
	@UniverseColumn(name="61.2")
	private BigDecimal assignedInterestReceived;
	
	@UniverseColumn(name="60.4")
	private BigDecimal assignedMisc1Owing;
	
	@UniverseColumn(name="61.4")
	private BigDecimal assignedMisc1Received;
	
	@UniverseColumn(name="60.5")
	private BigDecimal assignedMisc2Owing;
	
	@UniverseColumn(name="61.5")
	private BigDecimal assignedMisc2Received;
	
	@UniverseColumn(name="60.6")
	private BigDecimal assignedMisc3Owing;	
	
	@UniverseColumn(name="61.6")
	private BigDecimal assignedMisc3Received;	
	
	@UniverseColumn(name="60.7")
	private BigDecimal assignedMisc4Owing;	
	
	@UniverseColumn(name="61.7")
	private BigDecimal assignedMisc4Received;	
	
	@UniverseColumn(name="60.8")
	private BigDecimal assignedMisc5Owing;
	
	@UniverseColumn(name="61.8")
	private BigDecimal assignedMisc5Received;
	
	@UniverseColumn(name="60.9")
	private BigDecimal assignedMisc6Owing;
	
	@UniverseColumn(name="61.9")
	private BigDecimal assignedMisc6Received;
	
	@UniverseColumn(name="60.10")
	private BigDecimal assignedMisc7Owing;	
	
	@UniverseColumn(name="61.10")
	private BigDecimal assignedMisc7Received;
	
	@UniverseColumn(name="60.11")
	private BigDecimal assignedMisc8Owing;
	
	@UniverseColumn(name="61.11")
	private BigDecimal assignedMisc8Received;
	
	@UniverseColumn(name="60.12")
	private BigDecimal assignedMisc9Owing;	
	
	@UniverseColumn(name="61.12")
	private BigDecimal assignedMisc9Received;
	
	@UniverseColumn(name="18")
	private BigDecimal attorneyOwing;	
	
	@UniverseColumn(name="26")
	private BigDecimal attorneyReceived;	
	
	@UniverseColumn(name="76.2")
	private Date bankoReceivedDate;	
	
	@UniverseColumn(name="76.1")
	private Date bankoRequestDate;	
	
	@UniverseColumn(name="59.21")
	private String bankruptcyCode;
	
	@UniverseColumn(name="76.1")
	private Date bankruptcyDate;
	
	@UniverseColumn(name="46")
	private String cancelCreditRptFlag;	
	
	@UniverseColumn(name="25")
	private BigDecimal cancelledAmount;
	
	@UniverseColumn(name="52")
	private Date cancelledDate;	
	
	private Integer carrier1;
	
	private Integer carrier2;
	
	private Integer carrier3;
	
	@UniverseColumn(name="1")
	private String clientId;
	
	@UniverseColumn(name="34")
	private Date clientLastChargeLastPayDate;
	
	@UniverseColumn(name="57")
	private BigDecimal commissionRate;	
	
	@UniverseColumn(name="56")
	private Date costToDate;	
	
	@UniverseColumn(name="19")
	private BigDecimal courtOwing;	
	
	@UniverseColumn(name="27")
	private BigDecimal courtReceived;	
	
	@UniverseColumn(name="59.12")
	private Integer creditReportDelayDays;	
	
	@UniverseColumn(name="59.10")
	private Date deskChangeDate;
	
	@UniverseColumn(name="10")
	private String deskId;
	
	@UniverseColumn(name="59.14")
	private String disputeFlag;
	
	@UniverseColumn(name="24")
	private String forwardClientName;
	
	@UniverseColumn(name="59.6")
	private String fwdOldDeskID;
	
	@UniverseColumn(name="77")
	private Date firstDelinquencyDate;	
	
	@UniverseColumn(name="43.1")
	private BigDecimal grossCommission;	
	
	@UniverseColumn(name="43.2")
	private BigDecimal grossLessFwdFees;	
	
	@UniverseColumn(name="3.1")
	private String address1;
	
	@UniverseColumn(name="3.2")
	private String address2;
	
	@UniverseColumn(name="9")
	private Date birthDate;
	
	@UniverseColumn(name="4")
	private String city;
	

	@UniverseColumn(name="2.1")
	private String debtorName;
	
	@UniverseColumn(name="38")
	private String guarantorPersonKey;
	
	@UniverseColumn(name="8")
	private String phoneNumber;
	
	@UniverseColumn(name="20")
	private String socialSecurityNumber;
	
	@UniverseColumn(name="5")
	private String state;
	
	@UniverseColumn(name="6")
	private String zip;
	
	@UniverseColumn(name="68")
	private Date interestDate;	
	
	@UniverseColumn(name="17")
	private BigDecimal interestOwing;	
	
	@UniverseColumn(name="44")
	private BigDecimal interestRate;	
	
	@UniverseColumn(name="21")
	private BigDecimal interestReceived;	
	
	@UniverseColumn(name="36")
	private String invoiceNumber;
	
	private Long jobId;
		
	@UniverseColumn(name="42")
	private String lastActionCode;
	
	@UniverseColumn(name="22")
	private Date lastActionDate;
	
	@UniverseColumn(name="13")
	private Date lastPayDate;	
	
	@UniverseColumn(name="59.3")
	private String loadSource;
	
	@UniverseColumn(name="31")
	private BigDecimal miscOwing;	
	
	@UniverseColumn(name="32")
	private BigDecimal miscReceived;
	
	@UniverseColumn(name="59.16")
	private Date nsfDate;
	
	@UniverseColumn(name="63.14")
	private BigDecimal nonAssignAttorneyCommRate;
	
	@UniverseColumn(name="63.15")
	private BigDecimal nonAssignCourtCommRate;	
	
	@UniverseColumn(name="63.13")
	private BigDecimal nonAssignInterestCommRate;	
	
	@UniverseColumn(name="63.16")
	private BigDecimal nonAssignMisc1CommRate;	
	
	@UniverseColumn(name="63.17")
	private BigDecimal nonAssignMisc2CommRate;	
	
	@UniverseColumn(name="64.2")
	private BigDecimal nonAssignMisc2Owing;
	
	@UniverseColumn(name="65.2")
	private BigDecimal nonAssignMisc2Received;	
	
	@UniverseColumn(name="63.18")
	private BigDecimal nonAssignMisc3CommRate;	
	
	@UniverseColumn(name="64.3")
	private BigDecimal nonAssignMisc3Owing;	
	
	@UniverseColumn(name="65.3")
	private BigDecimal nonAssignMisc3Received;	
	
	@UniverseColumn(name="63.19")
	private BigDecimal nonAssignMisc4CommRate;	
	
	@UniverseColumn(name="64.4")
	private BigDecimal nonAssignMisc4Owing;	
	
	@UniverseColumn(name="65.4")
	private BigDecimal nonAssignMisc4Received;	
	
	@UniverseColumn(name="63.20")
	private BigDecimal nonAssignMisc5CommRate;	
	
	@UniverseColumn(name="64.5")
	private BigDecimal nonAssignMisc5Owing;	
	
	@UniverseColumn(name="65.5")
	private BigDecimal nonAssignMisc5Received;	
	
	@UniverseColumn(name="63.21")
	private BigDecimal nonAssignMisc6CommRate;	
	
	@UniverseColumn(name="64.6")
	private BigDecimal nonAssignMisc6Owing;	
	
	@UniverseColumn(name="65.6")
	private BigDecimal nonAssignMisc6Received;	
	
	@UniverseColumn(name="63.22")
	private BigDecimal nonAssignMisc7CommRate;
	
	@UniverseColumn(name="64.7")
	private BigDecimal nonAssignMisc7Owing;	
	
	@UniverseColumn(name="65.7")
	private BigDecimal nonAssignMisc7Received;	
	
	@UniverseColumn(name="63.23")
	private BigDecimal nonAssignMisc8CommRate;	
	
	@UniverseColumn(name="64.8")
	private BigDecimal nonAssignMisc8Owing;	
	
	@UniverseColumn(name="65.8")
	private BigDecimal nonAssignMisc8Received;	
	
	@UniverseColumn(name="63.24")
	private BigDecimal nonAssignMisc9CommRate;	
	
	@UniverseColumn(name="64.9")
	private BigDecimal nonAssignMisc9Owing;	
	
	@UniverseColumn(name="65.9")
	private BigDecimal nonAssignMisc9Received;	
	
	@UniverseColumn(name="33")
	private String noticeSeriesId;
	
	@UniverseColumn(name="37")
	private String oldAccountNumber;
	
	@UniverseColumn(name="59.9")
	private String oldDeskId;
	
	@UniverseColumn(name="66")
	private BigDecimal otherCancelledAmount;	
	
	@UniverseColumn(name="59.15")
	private String pifFlag;
	
	@UniverseColumn(name="7")
	private String packetId;

	@UniverseColumn(name="11")
	private String clientAccountNumber;

	@UniverseColumn(name="48")
	private Integer paymentPlanId;
	
	@UniverseColumn(name="69.1")
	private String previousStatusCode;
	
	@UniverseColumn(name="45")
	private BigDecimal principalAdjustment;
	
	@UniverseColumn(name="78.2")
	private String rpMemberType;
	
	@UniverseColumn(name="78.1")
	private Integer rpPacketId;	
	
	@UniverseColumn(name="29")
	private String referenceNumber;
	
	@UniverseColumn(name="59.5")
	private String reportedToBureauFlag;
	
	@UniverseColumn(name="28")
	private Date setupDate;	
	
	@UniverseColumn(name="69.2")
	private Date statusChangeDate;
	
	@UniverseColumn(name="23")
	private String statusCode;
	
	@UniverseColumn(name="62.11")
	private Date strategyDate;	
	
	@UniverseColumn(name="62.13")
	private String strategyId;
	
	@UniverseColumn(name="59.11")
	private String subtractCancelAmountFlag;

	private Boolean selfPay;
	
	private Boolean insurance;

	private BigDecimal assignedPrincipalOwing;
	
	private BigDecimal assignedPrincipalReceived;
	
	private BigDecimal nonAssignMisc1Owing;

	private BigDecimal nonAssignMisc1Received;
	
	private BigDecimal totalBalance;
	
	public DebtorDO(){
		super();
	}
	
	public Integer getDebtorNumber() {
		return debtorNumber;
	}

	public void setDebtorNumber(Integer debtorNumber) {
		this.debtorNumber = debtorNumber;
	}

	public Date getAccollaidReceiveDate() {
		return accollaidReceiveDate;
	}

	public void setAccollaidReceiveDate(Date accollaidReceiveDate) {
		this.accollaidReceiveDate = accollaidReceiveDate;				
	}

	public Date getAccollaidRequestDate() {
		return accollaidRequestDate;
	}

	public void setAccollaidRequestDate(Date accollaidRequestDate) {
		this.accollaidRequestDate = accollaidRequestDate;
	}

	public BigDecimal getAssignFeesCommRate() {
		return assignFeesCommRate;
	}

	public void setAssignFeesCommRate(BigDecimal assignFeesCommRate) {
		this.assignFeesCommRate = assignFeesCommRate;
	}

	public BigDecimal getAssignInterestCommRate() {
		return assignInterestCommRate;
	}

	public void setAssignInterestCommRate(BigDecimal assignInterestCommRate) {
		this.assignInterestCommRate = assignInterestCommRate;
	}

	public BigDecimal getAssignMisc1CommRate() {
		return assignMisc1CommRate;
	}

	public void setAssignMisc1CommRate(BigDecimal assignMisc1CommRate) {
		this.assignMisc1CommRate = assignMisc1CommRate;
	}

	public BigDecimal getAssignMisc2CommRate() {
		return assignMisc2CommRate;
	}

	public void setAssignMisc2CommRate(BigDecimal assignMisc2CommRate) {
		this.assignMisc2CommRate = assignMisc2CommRate;
	}

	public BigDecimal getAssignMisc3CommRate() {
		return assignMisc3CommRate;
	}

	public void setAssignMisc3CommRate(BigDecimal assignMisc3CommRate) {
		this.assignMisc3CommRate = assignMisc3CommRate;
	}

	public BigDecimal getAssignMisc4CommRate() {
		return assignMisc4CommRate;
	}

	public void setAssignMisc4CommRate(BigDecimal assignMisc4CommRate) {
		this.assignMisc4CommRate = assignMisc4CommRate;
	}

	public BigDecimal getAssignMisc5CommRate() {
		return assignMisc5CommRate;
	}

	public void setAssignMisc5CommRate(BigDecimal assignMisc5CommRate) {
		this.assignMisc5CommRate = assignMisc5CommRate;
	}

	public BigDecimal getAssignMisc6CommRate() {
		return assignMisc6CommRate;
	}

	public void setAssignMisc6CommRate(BigDecimal assignMisc6CommRate) {
		this.assignMisc6CommRate = assignMisc6CommRate;
	}

	public BigDecimal getAssignMisc7CommRate() {
		return assignMisc7CommRate;
	}

	public void setAssignMisc7CommRate(BigDecimal assignMisc7CommRate) {
		this.assignMisc7CommRate = assignMisc7CommRate;
	}

	public BigDecimal getAssignMisc8CommRate() {
		return assignMisc8CommRate;
	}

	public void setAssignMisc8CommRate(BigDecimal assignMisc8CommRate) {
		this.assignMisc8CommRate = assignMisc8CommRate;
	}

	public BigDecimal getAssignMisc9CommRate() {
		return assignMisc9CommRate;
	}

	public void setAssignMisc9CommRate(BigDecimal assignMisc9CommRate) {
		this.assignMisc9CommRate = assignMisc9CommRate;
	}

	public BigDecimal getAssignPrincipalCommRate() {
		return assignPrincipalCommRate;
	}

	public void setAssignPrincipalCommRate(BigDecimal assignPrincipalCommRate) {
		this.assignPrincipalCommRate = assignPrincipalCommRate;
	}

	public BigDecimal getAssignedAmountOwing() {
		return assignedAmountOwing;
	}

	public void setAssignedAmountOwing(BigDecimal assignedAmountOwing) {
		this.assignedAmountOwing = assignedAmountOwing;
	}

	public BigDecimal getAssignedAmountReceived() {
		return assignedAmountReceived;
	}

	public void setAssignedAmountReceived(BigDecimal assignedAmountReceived) {
		this.assignedAmountReceived = assignedAmountReceived;
	}

	public BigDecimal getAssignedCommFeesOwing() {
		return assignedCommFeesOwing;
	}

	public void setAssignedCommFeesOwing(BigDecimal assignedCommFeesOwing) {
		this.assignedCommFeesOwing = assignedCommFeesOwing;
	}

	public BigDecimal getAssignedCommFeesReceived() {
		return assignedCommFeesReceived;
	}

	public void setAssignedCommFeesReceived(BigDecimal assignedCommFeesReceived) {
		this.assignedCommFeesReceived = assignedCommFeesReceived;
	}

	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	public BigDecimal getAssignedInterestOwing() {
		return assignedInterestOwing;
	}

	public void setAssignedInterestOwing(BigDecimal assignedInterestOwing) {
		this.assignedInterestOwing = assignedInterestOwing;
	}

	public BigDecimal getAssignedInterestReceived() {
		return assignedInterestReceived;
	}

	public void setAssignedInterestReceived(BigDecimal assignedInterestReceived) {
		this.assignedInterestReceived = assignedInterestReceived;
	}

	public BigDecimal getAssignedMisc1Owing() {
		return assignedMisc1Owing;
	}

	public void setAssignedMisc1Owing(BigDecimal assignedMisc1Owing) {
		this.assignedMisc1Owing = assignedMisc1Owing;
	}

	public BigDecimal getAssignedMisc1Received() {
		return assignedMisc1Received;
	}

	public void setAssignedMisc1Received(BigDecimal assignedMisc1Received) {
		this.assignedMisc1Received = assignedMisc1Received;
	}

	public BigDecimal getAssignedMisc2Owing() {
		return assignedMisc2Owing;
	}

	public void setAssignedMisc2Owing(BigDecimal assignedMisc2Owing) {
		this.assignedMisc2Owing = assignedMisc2Owing;
	}

	public BigDecimal getAssignedMisc2Received() {
		return assignedMisc2Received;
	}

	public void setAssignedMisc2Received(BigDecimal assignedMisc2Received) {
		this.assignedMisc2Received = assignedMisc2Received;
	}

	public BigDecimal getAssignedMisc3Owing() {
		return assignedMisc3Owing;
	}

	public void setAssignedMisc3Owing(BigDecimal assignedMisc3Owing) {
		this.assignedMisc3Owing = assignedMisc3Owing;
	}

	public BigDecimal getAssignedMisc3Received() {
		return assignedMisc3Received;
	}

	public void setAssignedMisc3Received(BigDecimal assignedMisc3Received) {
		this.assignedMisc3Received = assignedMisc3Received;
	}

	public BigDecimal getAssignedMisc4Owing() {
		return assignedMisc4Owing;
	}

	public void setAssignedMisc4Owing(BigDecimal assignedMisc4Owing) {
		this.assignedMisc4Owing = assignedMisc4Owing;
	}

	public BigDecimal getAssignedMisc4Received() {
		return assignedMisc4Received;
	}

	public void setAssignedMisc4Received(BigDecimal assignedMisc4Received) {
		this.assignedMisc4Received = assignedMisc4Received;
	}

	public BigDecimal getAssignedMisc5Owing() {
		return assignedMisc5Owing;
	}

	public void setAssignedMisc5Owing(BigDecimal assignedMisc5Owing) {
		this.assignedMisc5Owing = assignedMisc5Owing;
	}

	public BigDecimal getAssignedMisc5Received() {
		return assignedMisc5Received;
	}

	public void setAssignedMisc5Received(BigDecimal assignedMisc5Received) {
		this.assignedMisc5Received = assignedMisc5Received;
	}

	public BigDecimal getAssignedMisc6Owing() {
		return assignedMisc6Owing;
	}

	public void setAssignedMisc6Owing(BigDecimal assignedMisc6Owing) {
		this.assignedMisc6Owing = assignedMisc6Owing;
	}

	public BigDecimal getAssignedMisc6Received() {
		return assignedMisc6Received;
	}

	public void setAssignedMisc6Received(BigDecimal assignedMisc6Received) {
		this.assignedMisc6Received = assignedMisc6Received;
	}

	public BigDecimal getAssignedMisc7Owing() {
		return assignedMisc7Owing;
	}

	public void setAssignedMisc7Owing(BigDecimal assignedMisc7Owing) {
		this.assignedMisc7Owing = assignedMisc7Owing;
	}

	public BigDecimal getAssignedMisc7Received() {
		return assignedMisc7Received;
	}

	public void setAssignedMisc7Received(BigDecimal assignedMisc7Received) {
		this.assignedMisc7Received =assignedMisc7Received;
	}

	public BigDecimal getAssignedMisc8Owing() {
		return assignedMisc8Owing;
	}

	public void setAssignedMisc8Owing(BigDecimal assignedMisc8Owing) {
		this.assignedMisc8Owing = assignedMisc8Owing;
	}

	public BigDecimal getAssignedMisc8Received() {
		return assignedMisc8Received;
	}

	public void setAssignedMisc8Received(BigDecimal assignedMisc8Received) {
		this.assignedMisc8Received = assignedMisc8Received;
	}

	public BigDecimal getAssignedMisc9Owing() {
		return assignedMisc9Owing;
	}

	public void setAssignedMisc9Owing(BigDecimal assignedMisc9Owing) {
		this.assignedMisc9Owing = assignedMisc9Owing;
	}

	public BigDecimal getAssignedMisc9Received() {
		return assignedMisc9Received;
	}

	public void setAssignedMisc9Received(BigDecimal assignedMisc9Received) {
		this.assignedMisc9Received = assignedMisc9Received;
	}

	public BigDecimal getAttorneyOwing() {
		return attorneyOwing;
	}

	public void setAttorneyOwing(BigDecimal attorneyOwing) {
		this.attorneyOwing = attorneyOwing;
	}

	public BigDecimal getAttorneyReceived() {
		return attorneyReceived;
	}

	public void setAttorneyReceived(BigDecimal attorneyReceived) {
		this.attorneyReceived = attorneyReceived;
	}

	public Date getBankoReceivedDate() {
		return bankoReceivedDate;
	}

	public void setBankoReceivedDate(Date bankoReceivedDate) {
		this.bankoReceivedDate = bankoReceivedDate;
	}

	public Date getBankoRequestDate() {
		return bankoRequestDate;
	}

	public void setBankoRequestDate(Date bankoRequestDate) {
		this.bankoRequestDate = bankoRequestDate;
	}

	public String getBankruptcyCode() {
		return bankruptcyCode;
	}

	public void setBankruptcyCode(String bankruptcyCode) {
		this.bankruptcyCode = bankruptcyCode;
	}

	public Date getBankruptcyDate() {
		return bankruptcyDate;
	}

	public void setBankruptcyDate(Date bankruptcyDate) {
		this.bankruptcyDate = bankruptcyDate;
	}

	public String getCancelCreditRptFlag() {
		return cancelCreditRptFlag;
	}

	public void setCancelCreditRptFlag(String cancelCreditRptFlag) {
		this.cancelCreditRptFlag = cancelCreditRptFlag;
	}

	public BigDecimal getCancelledAmount() {
		return cancelledAmount;
	}

	public void setCancelledAmount(BigDecimal cancelledAmount) {
		this.cancelledAmount = cancelledAmount;
	}

	public Date getCancelledDate() {
		return cancelledDate;
	}

	public void setCancelledDate(Date cancelledDate) {
		this.cancelledDate = cancelledDate;
	}

	public Integer getCarrier1(){
		return carrier1;
	}
	
	public void setCarrier1(Integer carrier1){
		this.carrier1=carrier1;
	}
	
	public Integer getCarrier2(){
		return carrier2;
	}
	
	public void setCarrier2(Integer carrier2)
	{
		this.carrier2=carrier2;
	}
	
	public Integer getCarrier3(){
		return carrier3;
	}
	
	public void setCarrier3(Integer carrier3){
		this.carrier3=carrier3;
	}
	
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Date getClientLastChargeLastPayDate() {
		return clientLastChargeLastPayDate;
	}

	public void setClientLastChargeLastPayDate(Date clientLastChargeLastPayDate) {
		this.clientLastChargeLastPayDate = clientLastChargeLastPayDate;
	}

	public BigDecimal getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(BigDecimal commissionRate) {
		this.commissionRate = commissionRate;
	}

	public Date getCostToDate() {
		return costToDate;
	}

	public void setCostToDate(Date costToDate) {
		this.costToDate = costToDate;
	}

	public BigDecimal getCourtOwing() {
		return courtOwing;
	}

	public void setCourtOwing(BigDecimal courtOwing) {
		this.courtOwing = courtOwing;
	}

	public BigDecimal getCourtReceived() {
		return courtReceived;
	}

	public void setCourtReceived(BigDecimal courtReceived) {
		this.courtReceived = courtReceived;
	}

	public Integer getCreditReportDelayDays() {
		return creditReportDelayDays;
	}

	public void setCreditReportDelayDays(Integer creditReportDelayDays) {
		this.creditReportDelayDays = creditReportDelayDays;
	}

	public Date getDeskChangeDate() {
		return deskChangeDate;
	}

	public void setDeskChangeDate(Date deskChangeDate) {
		this.deskChangeDate = deskChangeDate;
	}

	public String getDeskId() {
		return deskId;
	}

	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}

	public String getDisputeFlag() {
		return disputeFlag;
	}

	public void setDisputeFlag(String disputeFlag) {
		this.disputeFlag = disputeFlag;
	}

	public String getForwardClientName() {
		return forwardClientName;
	}

	public void setForwardClientName(String forwardClientName) {
		this.forwardClientName = forwardClientName;
	}

	public String getFwdOldDeskID() {
		return fwdOldDeskID;
	}

	public void setFwdOldDeskID(String fwdOldDeskID) {
		this.fwdOldDeskID = fwdOldDeskID;
	}

	public Date getFirstDelinquencyDate() {
		return firstDelinquencyDate;
	}

	public void setFirstDelinquencyDate(Date firstDelinquencyDate) {
		this.firstDelinquencyDate = firstDelinquencyDate;
	}

	public BigDecimal getGrossCommission() {
		return grossCommission;
	}

	public void setGrossCommission(BigDecimal grossCommission) {
		this.grossCommission = grossCommission;
	}

	public BigDecimal getGrossLessFwdFees() {
		return grossLessFwdFees;
	}

	public void setGrossLessFwdFees(BigDecimal grossLessFwdFees) {
		this.grossLessFwdFees = grossLessFwdFees;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDebtorName() {
		return debtorName;
	}
	
	public void setDebtorName(String debtorName) {
		this.debtorName = debtorName;
	}

	public String getGuarantorPersonKey() {
		return guarantorPersonKey;
	}

	public void setGuarantorPersonKey(String guarantorPersonKey) {
		this.guarantorPersonKey = guarantorPersonKey;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Date getInterestDate() {
		return interestDate;
	}

	public void setInterestDate(Date interestDate) {
		this.interestDate = interestDate;
	}

	public BigDecimal getInterestOwing() {
		return interestOwing;
	}

	public void setInterestOwing(BigDecimal interestOwing) {
		this.interestOwing = interestOwing;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getInterestReceived() {
		return interestReceived;		
	}

	public void setInterestReceived(BigDecimal interestReceived) {
		this.interestReceived = interestReceived;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}
	
	public Date getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate) {
		this.lastActionDate = lastActionDate;
	}
	
	public String getLastActionCode(){
		return lastActionCode;
	}
	
	public void setLastActionCode(String lastActionCode){
		this.lastActionCode = lastActionCode;
	}

	public Date getLastPayDate() {
		return lastPayDate;
	}

	public void setLastPayDate(Date lastPayDate) {
		this.lastPayDate = lastPayDate;
	}

	public String getLoadSource() {
		return loadSource;
	}

	public void setLoadSource(String loadSource) {
		this.loadSource = loadSource;
	}

	public BigDecimal getMiscOwing() {
		return miscOwing;
	}

	public void setMiscOwing(BigDecimal miscOwing) {
		this.miscOwing = miscOwing;
	}

	public BigDecimal getMiscReceived() {
		return miscReceived;
	}

	public void setMiscReceived(BigDecimal miscReceived) {
		this.miscReceived = miscReceived;
	}

	public Date getNsfDate() {
		return nsfDate;
	}

	public void setNsfDate(Date nsfDate) {
		this.nsfDate = nsfDate;
	}

	public BigDecimal getNonAssignAttorneyCommRate() {
		return nonAssignAttorneyCommRate;
	}

	public void setNonAssignAttorneyCommRate(BigDecimal nonAssignAttorneyCommRate) {
		this.nonAssignAttorneyCommRate = nonAssignAttorneyCommRate;
	}

	public BigDecimal getNonAssignCourtCommRate() {
		return nonAssignCourtCommRate;
	}

	public void setNonAssignCourtCommRate(BigDecimal nonAssignCourtCommRate) {
		this.nonAssignCourtCommRate = nonAssignCourtCommRate;
	}

	public BigDecimal getNonAssignInterestCommRate() {
		return nonAssignInterestCommRate;
	}

	public void setNonAssignInterestCommRate(BigDecimal nonAssignInterestCommRate) {
		this.nonAssignInterestCommRate = nonAssignInterestCommRate;
	}

	public BigDecimal getNonAssignMisc1CommRate() {
		return nonAssignMisc1CommRate;
	}

	public void setNonAssignMisc1CommRate(BigDecimal nonAssignMisc1CommRate) {
		this.nonAssignMisc1CommRate = nonAssignMisc1CommRate;
	}

	public BigDecimal getNonAssignMisc2CommRate() {
		return nonAssignMisc2CommRate;
	}

	public void setNonAssignMisc2CommRate(BigDecimal nonAssignMisc2CommRate) {
		this.nonAssignMisc2CommRate = nonAssignMisc2CommRate;
	}

	public BigDecimal getNonAssignMisc2Owing() {
		return nonAssignMisc2Owing;
	}

	public void setNonAssignMisc2Owing(BigDecimal nonAssignMisc2Owing) {
		this.nonAssignMisc2Owing = nonAssignMisc2Owing;
	}

	public BigDecimal getNonAssignMisc2Received() {
		return nonAssignMisc2Received;
	}

	public void setNonAssignMisc2Received(BigDecimal nonAssignMisc2Received) {
		this.nonAssignMisc2Received = nonAssignMisc2Received;
	}

	public BigDecimal getNonAssignMisc3CommRate() {
		return nonAssignMisc3CommRate;
	}

	public void setNonAssignMisc3CommRate(BigDecimal nonAssignMisc3CommRate) {
		this.nonAssignMisc3CommRate = nonAssignMisc3CommRate;
	}

	public BigDecimal getNonAssignMisc3Owing() {
		return nonAssignMisc3Owing;
	}

	public void setNonAssignMisc3Owing(BigDecimal nonAssignMisc3Owing) {
		this.nonAssignMisc3Owing = nonAssignMisc3Owing;
	}

	public BigDecimal getNonAssignMisc3Received() {
		return nonAssignMisc3Received;
	}

	public void setNonAssignMisc3Received(BigDecimal nonAssignMisc3Received) {
		this.nonAssignMisc3Received = nonAssignMisc3Received;
	}

	public BigDecimal getNonAssignMisc4CommRate() {
		return nonAssignMisc4CommRate;
	}

	public void setNonAssignMisc4CommRate(BigDecimal nonAssignMisc4CommRate) {
		this.nonAssignMisc4CommRate =nonAssignMisc4CommRate;
	}

	public BigDecimal getNonAssignMisc4Owing() {
		return nonAssignMisc4Owing;
	}

	public void setNonAssignMisc4Owing(BigDecimal nonAssignMisc4Owing) {
		this.nonAssignMisc4Owing =nonAssignMisc4Owing;
	}

	public BigDecimal getNonAssignMisc4Received() {
		return nonAssignMisc4Received;
	}

	public void setNonAssignMisc4Received(BigDecimal nonAssignMisc4Received) {
		this.nonAssignMisc4Received =nonAssignMisc4Received;
	}

	public BigDecimal getNonAssignMisc5CommRate() {
		return nonAssignMisc5CommRate;
	}

	public void setNonAssignMisc5CommRate(BigDecimal nonAssignMisc5CommRate) {
		this.nonAssignMisc5CommRate = nonAssignMisc5CommRate;
	}

	public BigDecimal getNonAssignMisc5Owing() {
		return nonAssignMisc5Owing;
	}

	public void setNonAssignMisc5Owing(BigDecimal nonAssignMisc5Owing) {
		this.nonAssignMisc5Owing = nonAssignMisc5Owing;
	}

	public BigDecimal getNonAssignMisc5Received() {
		return nonAssignMisc5Received;
	}

	public void setNonAssignMisc5Received(BigDecimal nonAssignMisc5Received) {
		this.nonAssignMisc5Received = nonAssignMisc5Received;
	}

	public BigDecimal getNonAssignMisc6CommRate() {
		return nonAssignMisc6CommRate;
	}

	public void setNonAssignMisc6CommRate(BigDecimal nonAssignMisc6CommRate) {
		this.nonAssignMisc6CommRate = nonAssignMisc6CommRate;
	}

	public BigDecimal getNonAssignMisc6Owing() {
		return nonAssignMisc6Owing;
	}

	public void setNonAssignMisc6Owing(BigDecimal nonAssignMisc6Owing) {
		this.nonAssignMisc6Owing = nonAssignMisc6Owing;
	}

	public BigDecimal getNonAssignMisc6Received() {
		return nonAssignMisc6Received;
	}

	public void setNonAssignMisc6Received(BigDecimal nonAssignMisc6Received) {
		this.nonAssignMisc6Received = nonAssignMisc6Received;
	}

	public BigDecimal getNonAssignMisc7CommRate() {
		return nonAssignMisc7CommRate;
	}

	public void setNonAssignMisc7CommRate(BigDecimal nonAssignMisc7CommRate) {
		this.nonAssignMisc7CommRate = nonAssignMisc7CommRate;
	}

	public BigDecimal getNonAssignMisc7Owing() {
		return nonAssignMisc7Owing;
	}

	public void setNonAssignMisc7Owing(BigDecimal nonAssignMisc7Owing) {
		this.nonAssignMisc7Owing = nonAssignMisc7Owing;
	}

	public BigDecimal getNonAssignMisc7Received() {
		return nonAssignMisc7Received;
	}

	public void setNonAssignMisc7Received(BigDecimal nonAssignMisc7Received) {
		this.nonAssignMisc7Received = nonAssignMisc7Received;
	}

	public BigDecimal getNonAssignMisc8CommRate() {
		return nonAssignMisc8CommRate;
	}

	public void setNonAssignMisc8CommRate(BigDecimal nonAssignMisc8CommRate) {
		this.nonAssignMisc8CommRate = nonAssignMisc8CommRate;
	}

	public BigDecimal getNonAssignMisc8Owing() {
		return nonAssignMisc8Owing;
	}

	public void setNonAssignMisc8Owing(BigDecimal nonAssignMisc8Owing) {
		this.nonAssignMisc8Owing = nonAssignMisc8Owing;
	}

	public BigDecimal getNonAssignMisc8Received() {
		return nonAssignMisc8Received;
	}

	public void setNonAssignMisc8Received(BigDecimal nonAssignMisc8Received) {
		this.nonAssignMisc8Received = nonAssignMisc8Received;
	}

	public BigDecimal getNonAssignMisc9CommRate() {
		return nonAssignMisc9CommRate;
	}

	public void setNonAssignMisc9CommRate(BigDecimal nonAssignMisc9CommRate) {
		this.nonAssignMisc9CommRate = nonAssignMisc9CommRate;
	}

	public BigDecimal getNonAssignMisc9Owing() {
		return nonAssignMisc9Owing;
	}

	public void setNonAssignMisc9Owing(BigDecimal nonAssignMisc9Owing) {
		this.nonAssignMisc9Owing = nonAssignMisc9Owing;
	}

	public BigDecimal getNonAssignMisc9Received() {
		return nonAssignMisc9Received;
	}

	public void setNonAssignMisc9Received(BigDecimal nonAssignMisc9Received) {
		this.nonAssignMisc9Received = nonAssignMisc9Received;
	}

	public String getNoticeSeriesId() {
		return noticeSeriesId;
	}

	public void setNoticeSeriesId(String noticeSeriesId) {
		this.noticeSeriesId = noticeSeriesId;
	}

	public String getOldAccountNumber() {
		return oldAccountNumber;
	}

	public void setOldAccountNumber(String oldAccountNumber) {
		this.oldAccountNumber = oldAccountNumber;
	}

	public String getOldDeskId() {
		return oldDeskId;
	}

	public void setOldDeskId(String oldDeskId) {
		this.oldDeskId = oldDeskId;
	}

	public BigDecimal getOtherCancelledAmount() {
		return otherCancelledAmount;
	}

	public void setOtherCancelledAmount(BigDecimal otherCancelledAmount) {
		this.otherCancelledAmount = otherCancelledAmount;
	}


	public String getPifFlag() {
		return pifFlag;
	}

	public void setPifFlag(String pifFlag) {
		this.pifFlag = pifFlag;
	}

	public String getPacketId() {
		return packetId;
	}

	public void setPacketId(String packetId) {
		this.packetId = packetId;
	}


	public String getClientAccountNumber() {
		return clientAccountNumber;
	}

	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Integer getPaymentPlanId() {
		return paymentPlanId;
	}

	public void setPaymentPlanId(Integer paymentPlanId) {
		this.paymentPlanId = paymentPlanId;
	}

	public String getPreviousStatusCode() {
		return previousStatusCode;
	}

	public void setPreviousStatusCode(String previousStatusCode) {
		this.previousStatusCode = previousStatusCode;
	}

	public BigDecimal getPrincipalAdjustment() {
		return principalAdjustment;
	}

	public void setPrincipalAdjustment(BigDecimal principalAdjustment) {
		this.principalAdjustment = principalAdjustment;
	}

	public String getRpMemberType() {
		return  rpMemberType;
	}

	public void setRpMemberType(String rpMemberType) {
		this.rpMemberType =rpMemberType;
	}

	public Integer getRpPacketId() {
		return rpPacketId;
	}

	public void setRpPacketId(Integer rpPacketId) {
		this.rpPacketId = rpPacketId;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReportedToBureauFlag() {
		return reportedToBureauFlag;
	}

	public void setReportedToBureauFlag(String reportedToBureauFlag) {
		this.reportedToBureauFlag = reportedToBureauFlag;
	}

	public Boolean getSelfPay(){
		if ("P".equals(rpMemberType)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	public void setSelfPay(Boolean selfPay){
		this.selfPay = selfPay;
	}
	
    public Boolean getInsurance(){
		if ("S".equals(rpMemberType)) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	public void setInsurance(Boolean insurance){  
		this.insurance = insurance;
	}

	public Date getSetupDate() {
		return setupDate;
	}

	public void setSetupDate(Date setupDate) {
		this.setupDate = setupDate;
	}

	public Date getStatusChangeDate() {
		return  statusChangeDate;
	}

	public void setStatusChangeDate(Date statusChangeDate) {
		this.statusChangeDate =statusChangeDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Date getStrategyDate() {
		return strategyDate;
	}

	public void setStrategyDate(Date strategyDate) {
		this.strategyDate = strategyDate;
	}

	public String getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(String strategyId) {
		this.strategyId = strategyId;
	}

	public String getSubtractCancelAmountFlag() {
		return subtractCancelAmountFlag;
	}

	public void setSubtractCancelAmountFlag(String subtractCancelAmountFlag) {
		this.subtractCancelAmountFlag = subtractCancelAmountFlag;
	}
	
	public BigDecimal getAssignedPrincipalOwing (){
		BigDecimal principalOwing=new BigDecimal("0");	
		principalOwing = assignedAmountOwing.subtract(assignedInterestOwing);
		principalOwing = principalOwing.subtract(assignedCommFeesReceived);
		principalOwing = principalOwing.subtract(assignedMisc1Owing);
		principalOwing = principalOwing.subtract(assignedMisc2Owing);
		principalOwing = principalOwing.subtract(assignedMisc3Owing);
		principalOwing = principalOwing.subtract(assignedMisc4Owing);
		principalOwing = principalOwing.subtract(assignedMisc5Owing);
		principalOwing = principalOwing.subtract(assignedMisc6Owing);
		principalOwing = principalOwing.subtract(assignedMisc7Owing);
		principalOwing = principalOwing.subtract(assignedMisc8Owing);
		principalOwing = principalOwing.subtract(assignedMisc9Owing);
		return principalOwing;
	}
	
	public void setAssignedPrincipalOwing(BigDecimal assignedPrincipalOwing){
		this.assignedPrincipalOwing = assignedPrincipalOwing;
	}
	
	public BigDecimal getAssignedPrincipalReceived(){
		BigDecimal principalReceived=new BigDecimal("0");
		principalReceived= assignedAmountReceived.subtract(assignedInterestReceived);
		principalReceived=principalReceived.subtract(assignedCommFeesReceived);
		principalReceived=principalReceived.subtract(assignedMisc1Received);
		principalReceived=principalReceived.subtract(assignedMisc2Received);
		principalReceived=principalReceived.subtract(assignedMisc3Received);
		principalReceived=principalReceived.subtract(assignedMisc4Received);
		principalReceived=principalReceived.subtract(assignedMisc5Received);
		principalReceived=principalReceived.subtract(assignedMisc6Received);
		principalReceived=principalReceived.subtract(assignedMisc7Received);
		principalReceived=principalReceived.subtract(assignedMisc8Received);
		principalReceived=principalReceived.subtract(assignedMisc9Received);
		return principalReceived;
	}

	public void setAssignedPrincipalReceived(BigDecimal assignedPrincipalReceived){
		this.assignedPrincipalReceived = assignedPrincipalReceived;
	}
	
	public BigDecimal getNonAssignMisc1Owing(){
		BigDecimal nonAssignMiscOwing=new BigDecimal("0");		
		nonAssignMiscOwing=miscOwing.subtract(nonAssignMisc2Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc3Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc4Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc5Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc6Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc7Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc8Owing);
		nonAssignMiscOwing=nonAssignMiscOwing.subtract(nonAssignMisc9Owing);
		return nonAssignMiscOwing;
	}
	
	public void setNonAssignMisc1Owing(BigDecimal nonAssignMisc1Owing){
		this.nonAssignMisc1Owing = nonAssignMisc1Owing;
	}

	public BigDecimal getNonAssignMisc1Received(){
		BigDecimal nonAssignMisc1Received=new BigDecimal("0");
		nonAssignMisc1Received=miscReceived.subtract(nonAssignMisc2Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc3Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc4Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc5Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc6Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc7Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc8Received);
		nonAssignMisc1Received=nonAssignMisc1Received.subtract(nonAssignMisc9Received);
		
		return nonAssignMisc1Received;
	}
	
	public void setNonAssignMisc1Received(BigDecimal nonAssignMisc1Received){
		this.nonAssignMisc1Received = nonAssignMisc1Received;
	}

	public BigDecimal getTotalBalance(){
		BigDecimal totalBalance=new BigDecimal("0");
		totalBalance=assignedAmountOwing.subtract(assignedAmountReceived);
		totalBalance=totalBalance.add(interestOwing);
		totalBalance=totalBalance.subtract(interestReceived);
		totalBalance=totalBalance.add(attorneyOwing);
		totalBalance=totalBalance.subtract(attorneyReceived);
		totalBalance=totalBalance.add(courtOwing);
		totalBalance=totalBalance.subtract(courtReceived);
		totalBalance=totalBalance.add(miscOwing);
		totalBalance=totalBalance.subtract(miscReceived);
		return totalBalance;
	}
	
	public void setTotalBalance(BigDecimal totalBalance){
		this.totalBalance = totalBalance;
	}
	
		
}




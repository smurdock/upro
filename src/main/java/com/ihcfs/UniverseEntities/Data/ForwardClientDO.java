/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import javax.persistence.*;

@Entity(name="PHPMFORWARDCLIENT")
@UniverseTable(name="FORWARD-CLIENT")

public class ForwardClientDO implements Serializable {
	
	private static final long serialVersionUID = -2199032785893698422L;

	@UniverseColumn(name="0")
	@Id
	private String forwardClientId;
	
	@UniverseColumn(name="2")
	private String address;

	@UniverseColumn(name="3")
	private String city;

	@UniverseColumn(name="35")
	private String clientId;

	@UniverseColumn(name="7")
	private String contact;

	@UniverseColumn(name="34")
	private String fax;

	@UniverseColumn(name="8")
	private BigDecimal forwardRate;

	@UniverseColumn(name="36")
	private String forwardStatusCodes;

	private Long jobId;
	
	@UniverseColumn(name="41")
	private String lastForwardFileSequence;

	@UniverseColumn(name="42")
	private String lastRecallFileSequence;

	@UniverseColumn(name="33")
	private Date lastUseDate;

	@UniverseColumn(name="1")
	private String name;

	@UniverseColumn(name="9")
	private String noteLine1;

	@UniverseColumn(name="10")
	private String noteLine2;

	@UniverseColumn(name="6")
	private String phone;

	@UniverseColumn(name="4")
	private String state;

	@UniverseColumn(name="5")
	private String zip;

	
	public String getForwardClientId() {
		return forwardClientId;
	}


	public void setForwardClientId(String forwardClientId) {
		this.forwardClientId = forwardClientId;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public BigDecimal getForwardRate() {
		return forwardRate;
	}


	public void setForwardRate(BigDecimal forwardRate) {
		this.forwardRate = forwardRate;
	}


	public String getForwardStatusCodes() {
		return forwardStatusCodes;
	}


	public void setForwardStatusCodes(String forwardStatusCodes) {
		this.forwardStatusCodes = forwardStatusCodes;
	}

	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}

	public String getLastForwardFileSequence() {
		return lastForwardFileSequence;
	}


	public void setLastForwardFileSequence(String lastForwardFileSequence) {
		this.lastForwardFileSequence = lastForwardFileSequence;
	}


	public String getLastRecallFileSequence() {
		return lastRecallFileSequence;
	}


	public void setLastRecallFileSequence(String lastRecallFileSequence) {
		this.lastRecallFileSequence = lastRecallFileSequence;
	}


	public Date getLastUseDate() {
		return lastUseDate;
	}


	public void setLastUseDate(Date lastUseDate) {
		this.lastUseDate = lastUseDate;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNoteLine1() {
		return noteLine1;
	}


	public void setNoteLine1(String noteLine1) {
		this.noteLine1 = noteLine1;
	}


	public String getNoteLine2() {
		return noteLine2;
	}


	public void setNoteLine2(String noteLine2) {
		this.noteLine2 = noteLine2;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public ForwardClientDO() {
		super();
	}
   
	
}

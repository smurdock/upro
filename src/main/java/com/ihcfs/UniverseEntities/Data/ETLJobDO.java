/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ETLJob
 *
 */
@Entity(name="PHPMETLJOB")

public class ETLJobDO implements Serializable {

	


	/**
	 * 
	 */
	private static final long serialVersionUID = 7989563223965351095L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)  
	private Long uniqueId;
	
	private String fileName;
	
	private String listName;
	
	private Date firstRunDate;
	
	private Date lastRunDate;
	
	private Date stopDate;
	
	private String errorMessage;
	
	private Date errorDate;
	
	private Integer loadCount;
	
	private Integer extractCount;
	
	private String lastId;
	
	private Date pingTime;
	
	private Boolean completed;
	
	public ETLJobDO() {
		super();
	}
	
	public Long getUniqueId() {
		return uniqueId;
	}
	
	public void setUniqueId(Long uniqueId){
		this.uniqueId=uniqueId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getFirstRunDate() {
		return firstRunDate;
	}

	public void setFirstRunDate(Date firstRunDate) {
		this.firstRunDate = firstRunDate;
	}

	public Date getLastRunDate() {
		return lastRunDate;
	}

	public void setLastRunDate(Date lastRunDate) {
		this.lastRunDate = lastRunDate;
	}
	
	public String getListName(){
		return listName;
	}
	
	public void setListName(String listName){
		this.listName=listName;
	}

	public Date getStopDate() {
		return stopDate;
	}

	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getErrorDate() {
		return errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

	public Integer getLoadCount() {
		return loadCount;
	}

	public void setLoadCount(Integer loadCount) {
		this.loadCount = loadCount;
	}

	public Integer getExtractCount(){
		return extractCount;
	}
	
	public void setExtractCount(Integer extractCount){
		this.extractCount=extractCount;
	}	
	
	public String getLastId() {
		return lastId;
	}

	public void setLastId(String lastId) {
		this.lastId = lastId;
	}

	public Date getPingTime() {
		return pingTime;
	}

	public void setPingTime(Date pingTime) {
		this.pingTime = pingTime;
	}
	
	public Boolean isCompleted(){
		return completed;
	}
	
	public void setCompleted(Boolean completed){
		this.completed=completed;
	}
   
}

/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity(name="PHPMPAYMENTHISTORY")
@Table(uniqueConstraints ={@UniqueConstraint(columnNames={"accountId","sequence"})})
@UniverseTable(name="PAYMENT-HISTORY")

public class PaymentHistoryDO implements Serializable {

	
	private static final long serialVersionUID = -3844324432878334491L;

/*	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)  
	private Long uniqueId;
	
	@UniverseColumn(name="0")
	private Integer accountId;
	
	private Integer sequence;*/
	
	@EmbeddedId
	protected PaymentHistoryPK paymentHistoryPK;
	
	@UniverseColumn(name="5")
	@MultiValuedColumn
	private String checkNumber;

	@UniverseColumn(name="11")
	@MultiValuedColumn
	private BigDecimal custom1Amount;

	@UniverseColumn(name="12")
	@MultiValuedColumn
	private BigDecimal custom2Amount;

	@UniverseColumn(name="13")
	@MultiValuedColumn
	private BigDecimal custom3Amount;

	@UniverseColumn(name="14")
	@MultiValuedColumn
	private Date custom1Date;

	@UniverseColumn(name="15")
	@MultiValuedColumn
	private Date custom2Date;

	@UniverseColumn(name="16")
	@MultiValuedColumn
	private Date custom3Date;

	@UniverseColumn(name="17")
	@MultiValuedColumn
	private String custom1Text;

	@UniverseColumn(name="18")
	@MultiValuedColumn
	private String custom2Text;

	@UniverseColumn(name="19")
	@MultiValuedColumn
	private String custom3Text;

	@UniverseColumn(name="20")
	@MultiValuedColumn
	private String custom4Text;

	@UniverseColumn(name="21")
	@MultiValuedColumn
	private String custom5Text;

	@UniverseColumn(name="22")
	@MultiValuedColumn
	private String custom6Text;

	@UniverseColumn(name="23")
	@MultiValuedColumn
	private String custom7Text;

	@UniverseColumn(name="24")
	@MultiValuedColumn
	private String custom8Text;

	@UniverseColumn(name="25")
	@MultiValuedColumn
	private String custom9Text;

	@UniverseColumn(name="26")
	@MultiValuedColumn
	private String custom10Text;

	@UniverseColumn(name="27")
	@MultiValuedColumn
	private String custom11Text;

	@UniverseColumn(name="28")
	@MultiValuedColumn
	private String custom12Text;

	@UniverseColumn(name="29")
	@MultiValuedColumn
	private String custom13Text;

	@UniverseColumn(name="30")
	@MultiValuedColumn
	private String custom14Text;

	@UniverseColumn(name="33")
	@MultiValuedColumn
	private String financialClass;

	@UniverseColumn(name="8")
	@MultiValuedColumn
	private String insuranceAccountId;

	private Long jobId;
	
	@UniverseColumn(name="9")
	@MultiValuedColumn
	private String nsfFlag;

	@UniverseColumn(name="31")
	@MultiValuedColumn
	private String payerId;

	@UniverseColumn(name="7")
	@MultiValuedColumn
	private Date stmtSeriesDate;

	@UniverseColumn(name="4")
	@MultiValuedColumn
	private BigDecimal transactionAmount;

	@UniverseColumn(name="2")
	@MultiValuedColumn
	private String transactionCode;

	@UniverseColumn(name="3")
	@MultiValuedColumn
	private Date transactionDate;

	@UniverseColumn(name="1")
	@MultiValuedColumn
	private String transactionId;

	@UniverseColumn(name="36")
	@MultiValuedColumn
	private String transactionNote;

	@UniverseColumn(name="6")
	@MultiValuedColumn
	private String transactionType;

	
	public PaymentHistoryDO() {
		super();
	}


	/*public Long getUniqueId() {
		return uniqueId;
	}


	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}


	public Integer getDebtorNumber() {
		return accountId;
	}


	public void setDebtorNumber(Integer accountId) {
		this.accountId = accountId;
	}


	public Integer getSequence() {
		return sequence;
	}


	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}*/


	public String getCheckNumber() {
		return checkNumber;
	}


	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}


	public BigDecimal getCustom1Amount() {
		return custom1Amount;
	}


	public void setCustom1Amount(BigDecimal custom1Amount) {
		this.custom1Amount = custom1Amount;
	}


	public BigDecimal getCustom2Amount() {
		return custom2Amount;
	}


	public void setCustom2Amount(BigDecimal custom2Amount) {
		this.custom2Amount = custom2Amount;
	}


	public BigDecimal getCustom3Amount() {
		return custom3Amount;
	}


	public void setCustom3Amount(BigDecimal custom3Amount) {
		this.custom3Amount = custom3Amount;
	}


	public Date getCustom1Date() {
		return custom1Date;
	}


	public void setCustom1Date(Date custom1Date) {
		this.custom1Date = custom1Date;
	}


	public Date getCustom2Date() {
		return custom2Date;
	}


	public void setCustom2Date(Date custom2Date) {
		this.custom2Date = custom2Date;
	}


	public Date getCustom3Date() {
		return custom3Date;
	}


	public void setCustom3Date(Date custom3Date) {
		this.custom3Date = custom3Date;
	}


	public String getCustom1Text() {
		return custom1Text;
	}


	public void setCustom1Text(String custom1Text) {
		this.custom1Text = custom1Text;
	}


	public String getCustom2Text() {
		return custom2Text;
	}


	public void setCustom2Text(String custom2Text) {
		this.custom2Text = custom2Text;
	}


	public String getCustom3Text() {
		return custom3Text;
	}


	public void setCustom3Text(String custom3Text) {
		this.custom3Text = custom3Text;
	}


	public String getCustom4Text() {
		return custom4Text;
	}


	public void setCustom4Text(String custom4Text) {
		this.custom4Text = custom4Text;
	}


	public String getCustom5Text() {
		return custom5Text;
	}


	public void setCustom5Text(String custom5Text) {
		this.custom5Text = custom5Text;
	}


	public String getCustom6Text() {
		return custom6Text;
	}


	public void setCustom6Text(String custom6Text) {
		this.custom6Text = custom6Text;
	}


	public String getCustom7Text() {
		return custom7Text;
	}


	public void setCustom7Text(String custom7Text) {
		this.custom7Text = custom7Text;
	}


	public String getCustom8Text() {
		return custom8Text;
	}


	public void setCustom8Text(String custom8Text) {
		this.custom8Text = custom8Text;
	}


	public String getCustom9Text() {
		return custom9Text;
	}


	public void setCustom9Text(String custom9Text) {
		this.custom9Text = custom9Text;
	}


	public String getCustom10Text() {
		return custom10Text;
	}


	public void setCustom10Text(String custom10Text) {
		this.custom10Text = custom10Text;
	}


	public String getCustom11Text() {
		return custom11Text;
	}


	public void setCustom11Text(String custom11Text) {
		this.custom11Text = custom11Text;
	}


	public String getCustom12Text() {
		return custom12Text;
	}


	public void setCustom12Text(String custom12Text) {
		this.custom12Text = custom12Text;
	}


	public String getCustom13Text() {
		return custom13Text;
	}


	public void setCustom13Text(String custom13Text) {
		this.custom13Text = custom13Text;
	}


	public String getCustom14Text() {
		return custom14Text;
	}


	public void setCustom14Text(String custom14Text) {
		this.custom14Text = custom14Text;
	}


	public String getFinancialClass() {
		return financialClass;
	}


	public void setFinancialClass(String financialClass) {
		this.financialClass = financialClass;
	}


	public String getInsuranceAccountId() {
		return insuranceAccountId;
	}


	public void setInsuranceAccountId(String insuranceAccountId) {
		this.insuranceAccountId = insuranceAccountId;
	}

	public Long getJobId(){
		return jobId;
	}
	
	public void setJobId(Long jobId){
		this.jobId=jobId;
	}

	public String getNsfFlag() {
		return nsfFlag;
	}


	public void setNsfFlag(String nsfFlag) {
		this.nsfFlag = nsfFlag;
	}


	public String getPayerId() {
		return payerId;
	}


	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}
	
	public PaymentHistoryPK getPaymentHistoryPK(){
		return paymentHistoryPK;
	}
	
	public void setPaymentHistoryPK(PaymentHistoryPK paymentHistoryPK){
		this.paymentHistoryPK=paymentHistoryPK;
	}

	public Date getStmtSeriesDate() {
		return stmtSeriesDate;
	}


	public void setStmtSeriesDate(Date stmtSeriesDate) {
		this.stmtSeriesDate = stmtSeriesDate;
	}


	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public String getTransactionCode() {
		return transactionCode;
	}


	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}


	public Date getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public String getTransactionNote() {
		return transactionNote;
	}


	public void setTransactionNote(String transactionNote) {
		this.transactionNote = transactionNote;
	}


	public String getTransactionType() {
		return transactionType;
	}


	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	
}

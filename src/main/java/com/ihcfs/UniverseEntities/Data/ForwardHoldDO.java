/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import javax.persistence.*;

@Entity(name="PHPMFORWARDHOLD")
@UniverseTable(name="FORWARD-HOLD")

public class ForwardHoldDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4505591203369108092L;

	@UniverseColumn(name="0")
	@Id
	private Integer accountId;
	
	@UniverseColumn(name="7")
	private Date approvalDate;

	@UniverseColumn(name="10")
	private String forwardAuthEmpInitials;

	@UniverseColumn(name="1")
	private String forwardClientId;

	@UniverseColumn(name="6")
	private String forwardFileSequence;

	@UniverseColumn(name="2")
	private Date forwardRequestDate;

	@UniverseColumn(name="3")
	private String forwardRequestEmpInitials;

	@UniverseColumn(name="8")
	private String forwardStatusCode;
	
	private Integer jobId;

	@UniverseColumn(name="11")
	private Date recallApprovalDate;

	@UniverseColumn(name="15")
	private String recallCancelEmpInitials;

	@UniverseColumn(name="14")
	private String recallEmpInitials;

	@UniverseColumn(name="12")
	private Date recallRequestDate;

	@UniverseColumn(name="5")
	private BigDecimal transferAmount;

	
	public ForwardHoldDO() {
		super();
	}

	
	public Integer getAccountId() {
		return accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public Date getApprovalDate() {
		return approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public String getForwardAuthEmpInitials() {
		return forwardAuthEmpInitials;
	}


	public void setForwardAuthEmpInitials(String forwardAuthEmpInitials) {
		this.forwardAuthEmpInitials = forwardAuthEmpInitials;
	}


	public String getForwardClientId() {
		return forwardClientId;
	}


	public void setForwardClientId(String forwardClientId) {
		this.forwardClientId = forwardClientId;
	}


	public String getForwardFileSequence() {
		return forwardFileSequence;
	}


	public void setForwardFileSequence(String forwardFileSequence) {
		this.forwardFileSequence = forwardFileSequence;
	}


	public Date getForwardRequestDate() {
		return forwardRequestDate;
	}


	public void setForwardRequestDate(Date forwardRequestDate) {
		this.forwardRequestDate = forwardRequestDate;
	}


	public String getForwardRequestEmpInitials() {
		return forwardRequestEmpInitials;
	}


	public void setForwardRequestEmpInitials(String forwardRequestEmpInitials) {
		this.forwardRequestEmpInitials = forwardRequestEmpInitials;
	}


	public String getForwardStatusCode() {
		return forwardStatusCode;
	}


	public void setForwardStatusCode(String forwardStatusCode) {
		this.forwardStatusCode = forwardStatusCode;
	}

	public Integer getJobId(){
		return jobId;
	}

	public void setJobId(Integer jobId){
		this.jobId=jobId;
	}

	public Date getRecallApprovalDate() {
		return recallApprovalDate;
	}


	public void setRecallApprovalDate(Date recallApprovalDate) {
		this.recallApprovalDate = recallApprovalDate;
	}


	public String getRecallCancelEmpInitials() {
		return recallCancelEmpInitials;
	}


	public void setRecallCancelEmpInitials(String recallCancelEmpInitials) {
		this.recallCancelEmpInitials = recallCancelEmpInitials;
	}


	public String getRecallEmpInitials() {
		return recallEmpInitials;
	}


	public void setRecallEmpInitials(String recallEmpInitials) {
		this.recallEmpInitials = recallEmpInitials;
	}


	public Date getRecallRequestDate() {
		return recallRequestDate;
	}


	public void setRecallRequestDate(Date recallRequestDate) {
		this.recallRequestDate = recallRequestDate;
	}


	public BigDecimal getTransferAmount() {
		return transferAmount;
	}


	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	
}

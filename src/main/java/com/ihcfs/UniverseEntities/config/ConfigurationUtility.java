/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.properties.PropertyValueEncryptionUtils;


public class ConfigurationUtility {
	private static String key="jasypt1239871237efg";
	
	private static Properties properties;
	
	public static String getProperty(String property) throws Exception
	{
		if (properties==null)
		{
			StandardPBEStringEncryptor stringEncryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
			stringEncryptor.setPassword(key);			
			properties = new EncryptableProperties(stringEncryptor);			
		}
		String filePath=System.getProperty("settings.dir");
		if (filePath==null)
		{
			String osName=System.getProperty("os.name");
			if (osName.toUpperCase().contains("WIN"))
			{
				filePath="C:\\Applications\\upro\\conf\\universe.properties";
			}
			else
			{
				filePath="/opt/upro/conf/universe.properties";
			}
		}
		
		else
		{

			String slash;
			String osName=System.getProperty("os.name");			
			if (osName.toUpperCase().contains("WIN"))			{

				slash="\\";//escape the slash since a backslash is reserved
			}
			
			else
			{
				slash="/";
			}
			
			if (!filePath.endsWith(slash))
			{
				filePath=filePath+slash;
			}
			filePath=filePath+"UniverseEntities"+slash+"UniverseEntities.properties";
		}

		InputStream propInput = (InputStream) new FileInputStream(filePath);
		properties.load(propInput);
		String val=properties.getProperty(property);
		propInput.close();
		return val;	
		
	}
		
	public static String getKey()
	{
		return key;
	}
	
	public static String getProperty(String property, String defaultValue) throws Exception
	{
		String value = getProperty(property);
		if (value==null)
		{
			value = defaultValue;
		}
		
		return value;
	}
	
}

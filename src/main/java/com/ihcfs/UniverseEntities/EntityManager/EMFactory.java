/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.EntityManager;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.ihcfs.UniverseEntities.config.ConfigurationUtility;


public abstract class EMFactory {
	
	private static Map dbProps;
	
	private static EntityManagerFactory emf;
	
	
	public static EntityManager createEntityManager() throws Exception
	{
		dbProps = new HashMap();
		dbProps.put("hibernate.connection.password",ConfigurationUtility.getProperty("db.encrypted.password"));
		emf = Persistence.createEntityManagerFactory("MMedArchive", dbProps);
		return emf.createEntityManager();
	}
	
	public static void closeEntityManager(){
		emf.close();
	}
	
	public static Session createSession()
	{
		SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		return sessionFactory.getCurrentSession();		
	}
	
}

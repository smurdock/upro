/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.EntityManager;

import asjava.uniobjects.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ihcfs.UniverseEntities.Data.*;
import com.ihcfs.UniverseEntities.PickUtilities.TimeDateConverter;
import com.ihcfs.UniverseEntities.config.ConfigurationUtility;

import asjava.uniclientlibs.UniDynArray;
import asjava.uniclientlibs.UniString;
import asjava.uniclientlibs.UniTokens;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.EmbeddedId;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.jcraft.jsch.*;
import jcifs.smb.*;


public class UniverseEntityManager {

private UniSession uniSession;
private Session remoteShellSession;
private String sessionType;
private Map<String,UniFile> fileMap;
private Map<String,UniSelectList> selectMap;
private Map<String,Field> compositeKeyMap;//key=table name, value = map with composite key fields (EX: accountId, sequenceId) 
private Map<String,TreeMap<String,Field>> universeFieldMap;//key=table name, value = map of universe fields.
private Map<String,TreeMap<String,Field>> entityFieldMap;//key=table name, value = map of entity fields.
private Map<String,TreeMap<String,Boolean>> multiValuedMapMap;//key=table name, value = map of booleans
private Map<String,TreeMap<String,Integer>> repeatingMapMap;//key=table name, value = map of booleans
private Boolean inUse;
private Gson gson = new Gson();
public static final String SESSION_TYPE_SSH =  "SESSION_TYPE_SSH";
public static final String SESSION_TYPE_UNIOBJECTS = "SESSION_TYPE_UNIOBJECTS";
private JSch jsch=new JSch();
private String hostName;
private String accountPath;
private String uvAccountPath;


	public UniverseEntityManager(String username, String password, String sessionType) throws Exception{
	this.sessionType=sessionType;

	hostName=ConfigurationUtility.getProperty("session.hostname");
	accountPath=ConfigurationUtility.getProperty("session.path");
	uvAccountPath = ConfigurationUtility.getProperty("uv.path");

	if(SESSION_TYPE_UNIOBJECTS.equals(sessionType)) {

		uniSession = new UniSession();
		uniSession.setHostPort(Integer.valueOf(ConfigurationUtility.getProperty("session.port")));
		uniSession.setUserName(username);
		uniSession.setPassword(password);
		uniSession.setHostName(hostName);
		uniSession.setAccountPath(accountPath);
		uniSession.setDataSourceType(ConfigurationUtility.getProperty("session.datasource.type"));
		uniSession.connect();
		initialize();
	} else if (SESSION_TYPE_SSH.equals(sessionType)){
		UniverseUserInfo userInfo = new UniverseUserInfo();
		userInfo.setPassword(password);
		remoteShellSession=jsch.getSession(username,ConfigurationUtility.getProperty("session.hostname"),22 );
		remoteShellSession.setUserInfo(userInfo);
		remoteShellSession.connect();
		initialize();
	} else{
		throw new Exception("Unsupported Universe Session Type: "+sessionType+". Try: "+SESSION_TYPE_SSH+" or "+SESSION_TYPE_UNIOBJECTS);
	}
}

public UniverseEntityManager() throws Exception
{
	sessionType=SESSION_TYPE_UNIOBJECTS;
	uniSession = new UniSession();
	uniSession.setHostPort(Integer.valueOf(ConfigurationUtility.getProperty("session.port")));
	uniSession.setUserName(ConfigurationUtility.getProperty("session.username"));
	String password=ConfigurationUtility.getProperty("session.encrypted.password");
	uniSession.setPassword(password);
	uniSession.setHostName(ConfigurationUtility.getProperty("session.hostname"));
	uniSession.setAccountPath(ConfigurationUtility.getProperty("session.path"));
	uniSession.setDataSourceType(ConfigurationUtility.getProperty("session.datasource.type"));
	uniSession.connect();
	accountPath=ConfigurationUtility.getProperty("session.path");
	uvAccountPath=ConfigurationUtility.getProperty("uv.path");
	initialize();
}

public UniverseEntityManager(UniSession uniSession)
{
	this.uniSession=uniSession;
	initialize();
}

public UniSession getUniSession(){
	return uniSession;
}


public void setUniSession(UniSession uniSession){
	this.uniSession = uniSession;
}

public Boolean isInUse(){
	return inUse;
}

public void setInUse(Boolean inUse){
	this.inUse=inUse;
}

private void initialize()
{
	fileMap = new HashMap<String,UniFile>();
	selectMap=new HashMap<String,UniSelectList>();
	compositeKeyMap = new TreeMap<String,Field>();
	universeFieldMap = new TreeMap<String,TreeMap<String,Field>>();
	entityFieldMap = new TreeMap<String,TreeMap<String,Field>>();
	multiValuedMapMap= new TreeMap<String,TreeMap<String,Boolean>>();
	repeatingMapMap = new TreeMap<String,TreeMap<String,Integer>>();	
}

public void close() throws UniFileException , UniSelectListException, UniSessionException //close file references
{
	setInUse(false);

	if(SESSION_TYPE_UNIOBJECTS.equals(sessionType)) {

		for (UniSelectList list : selectMap.values()) {
			list.clearList();
		}

		selectMap.clear();

		for (UniFile file : fileMap.values()) {
			file.close();
		}

		fileMap.clear();

		getUniSession().disconnect();
	} else if (SESSION_TYPE_SSH.equals(sessionType)){
		remoteShellSession.disconnect();
	}

}

public void release()
{
	setInUse(false);
}
	
public <T> List <T> find (Class <T> objectType, Class<T> persistType, String itemId) throws Exception
{
		Annotation annotation = objectType.getAnnotation(UniverseTable.class);
		UniverseTable table = (UniverseTable) annotation;
		UniDynArray record = getRecord(table.name(),itemId);
		return createEntityFromArray(objectType,persistType,record, itemId);
}

public <T> List <T> findFromJSON (Class <T> objectType, Class<T> persistType, String itemId) throws Exception
{
		Annotation annotation = objectType.getAnnotation(UniverseTable.class);
		UniverseTable table = (UniverseTable) annotation;
		UniDynArray record = getEntireRecordFromJSON(table.name(), itemId);
		return createEntityFromArray(objectType,persistType,record, itemId);
}

public SmbFileInputStream getSelectSamba(String listName) throws Exception
{
	String universeHost=ConfigurationUtility.getProperty("session.hostname");
	String universeUser=ConfigurationUtility.getProperty("session.username");
	jcifs.Config.setProperty( "jcifs.netbios.wins", universeHost );
	String accountPath = ConfigurationUtility.getProperty("session.path");
	String smbAccountPath=accountPath.replaceAll("D:", "");
	smbAccountPath=smbAccountPath.replaceAll("\\\\", "/");
	if (smbAccountPath.startsWith("/"))
	{
		smbAccountPath=smbAccountPath.replaceFirst("/","");
	}
	
	smbAccountPath="smb://"+universeHost+";"+universeUser+":"+ConfigurationUtility.getProperty("session.encrypted.password")+"@"+universeHost+"/"+smbAccountPath;
	
	if (!smbAccountPath.endsWith("/"))//be sure directory ends with a forward slash
	{
		smbAccountPath+="/";
	}	
	
	String sambaSaveListFilePath=smbAccountPath+"&SAVEDLISTS&/"+listName;
	
	SmbFileInputStream in = new SmbFileInputStream(sambaSaveListFilePath);
	
	return in;
}

public UniSelectList getSelect(String listName) throws Exception
{

	UniSelectList select=null;
	Boolean listFound=false;
	
	try
	{
	Iterator<UniSelectList> listIterator=selectMap.values().iterator();
	int i=0;
	while(listFound==false && i<10)//there are 11 available select lists for each Universe uniSession, numbered 0 through 10
	{
		if(!selectMap.isEmpty())		
		{
			select = listIterator.next();
		}
		
		if (select==null)
		{
			listFound=true;
			select = uniSession.selectList(i);
			select.getList(listName);
			selectMap.put(listName, select);
		}
		i++;
	}
	}
	catch (Exception e)
	{
		throw new Exception ("Universe RPC unable to get list "+listName+". Try checking if the list exists.",e);

	}	
	
	if (select==null)
	{
		throw new Exception ("No available select lists, UniverseEntityManager has reached the max of 11 active selects. Try setting isSambaListReadActive=true on the ETLService object, or using the UniverseEntityManager.getSambaSelect() method");
	}

	return select;
}

public void selectFile (String fileName) throws Exception{
//	UniCommand command =uniSession.command();
//	command.setCommand("SELECT "+fileName+UniTokens.FM+"SAVE-LIST SEAN");
//	command.exec();
//	command.wait();
	UniFile file = uniSession.openFile(fileName);
	UniString string =file.read();
	System.out.println(string.toString());
	file.close();

}

public void closeSelect(String listName) throws Exception
{
	UniSelectList list = selectMap.get(listName);
	if (list!=null)
	{
		list.clearList();
		selectMap.remove(listName);		
	}
}


private Float fieldAsFloat(String fieldName)
{
	Float fieldAsFloat=null;
	if (fieldName.split("\\.").length==1)
	{
		fieldAsFloat=Float.valueOf(fieldName);
	}
	
	else if (fieldName.split("\\.").length==2)
	{
		String wholeNumber=fieldName.split("\\.")[0];
		Integer ones = Integer.valueOf(wholeNumber);
		String fraction = fieldName.split("\\.")[1];
		Integer hundredths = Integer.valueOf(fraction);
		fieldAsFloat=ones+(hundredths*.01F);		
	}
	return fieldAsFloat;
}

private <T> List <T> createEntityFromArray (Class <T> sourceObjectType, Class<T> destinationObjectType,UniDynArray record, String itemId) throws Exception
{
	Set<Map.Entry<String, Field>> fieldSet=null;
	TreeMap<String, Field> uvFieldMap = universeFieldMap.get(sourceObjectType.getName());
	TreeMap<String,Boolean> multiValuedMap = multiValuedMapMap.get(sourceObjectType.getName());
	TreeMap<String,Field> sqlFieldMap = entityFieldMap.get(sourceObjectType.getName());
	TreeMap<String,Integer> repeatingFieldMap = repeatingMapMap.get(sourceObjectType.getName());
	Field compositeKey=null;//for multi-valued fields, a composite key is used consisting of the record id of the source record, combined with the value position
	Field pickRecordId=null;
	Field valueSequence=null;
	
	
	if (uvFieldMap == null)
	{
		uvFieldMap = new TreeMap<String,Field>();
		universeFieldMap.put(sourceObjectType.getName(), uvFieldMap);
	}
	
	if (sqlFieldMap == null)
	{
		sqlFieldMap = new TreeMap<String,Field>();
		entityFieldMap.put(sourceObjectType.getName(),sqlFieldMap);
	}
	
	if (multiValuedMap==null)
	{
		multiValuedMap = new TreeMap<String,Boolean>();
		multiValuedMapMap.put(sourceObjectType.getName(), multiValuedMap);
	}
	
	
	if (repeatingFieldMap==null)
	{
		repeatingFieldMap = new TreeMap<String,Integer>();
		repeatingMapMap.put(sourceObjectType.getName(), repeatingFieldMap);
	}
					
	if (uvFieldMap.isEmpty())
	{
		for (Field field:sourceObjectType.getDeclaredFields())
		{

			Boolean hasGeneratedValue = false;
			Boolean isId = false;
			
			sqlFieldMap.put(field.getName(), field);
			
			
			for (Annotation annotation:field.getAnnotations())
			{
				if (annotation instanceof EmbeddedId)
				{
					compositeKey=field;
					for (Field compositeField:field.getType().getDeclaredFields())//iterate over the primary key object
					{
						compositeField.setAccessible(true);
						
						if (compositeField.getName().contains("sequence"))
						{
							valueSequence=field;							
						}		
											
					}
					
					compositeKeyMap.put(sourceObjectType.getName(), field);//this is the field we must instantiate to create the composite
				}
				
				if (annotation instanceof UniverseColumn)
				{
					field.setAccessible(true);
					uvFieldMap.put((((UniverseColumn) annotation).name()), field);
				}
				
				else if (annotation instanceof Repeating)
				{
					repeatingFieldMap.put(field.getName(),Integer.valueOf(((Repeating)annotation).firstAttribute()));
				}
				
				else if (annotation instanceof MultiValuedColumn)
				{
					multiValuedMap.put(field.getName(), true);
				}
				
				else if (annotation instanceof GeneratedValue)
				{
					hasGeneratedValue=true;
				}
				
				else if (annotation instanceof Id)
				{
					isId=true;
				}
			}
			

		}
		

		pickRecordId= uvFieldMap.get("0");
		if (pickRecordId==null && valueSequence==null)
		{
			throw new Exception("UniverseEntityManager.createEntity failed because "+sourceObjectType.getName()+" is missing a record Id. Try adding a column with @UniverseColumn(name=\"0\") annotation.");
		}
		
		if (!multiValuedMap.isEmpty() && valueSequence==null)
		{
			throw new Exception("UniverseEntityManager.createEntity failed because "+sourceObjectType.getName()+" has multi-valued fields but it's missing a sequence. Try adding a column with @Embedded annotation.");
		}		
		
	}
	
		
	fieldSet = uvFieldMap.entrySet();		
	List <T> list = new ArrayList <T>();	
	T object = sourceObjectType.newInstance();
	
	if (sourceObjectType==destinationObjectType)// in this case the list consists of one object because we have a one to one relationship (one persistence record for each Universe Record)
	{
		list.add(object);
	}

	Class<?> keyClass = null;
	Field keyField = null;

	
	if(!compositeKeyMap.isEmpty())
	{
		keyField = compositeKeyMap.get(sourceObjectType.getName());
		keyClass = compositeKeyMap.get(sourceObjectType.getName()).getType();	
	}	
	
	if (keyField!=null)//multi-valued record 
	{


		for (Field field:keyClass.getDeclaredFields())
		{
			for (Annotation annotation:field.getAnnotations())
			{
				if (annotation instanceof UniverseColumn)
				{
					UniverseColumn column = (UniverseColumn) annotation;
					if("0".equals(column.name()))//record id
					{
						pickRecordId=field;
						field.setAccessible(true);						

					}
				}
			}

			if (field.getName().contains("sequence"))//value position
			{
				field.setAccessible(true);
				valueSequence=field;
			}
		}

	}		
	
	Iterator<Map.Entry<String,Field>> i = fieldSet.iterator();
	
	
	while (i.hasNext())
	{
		Map.Entry<String,Field> entry = (Map.Entry<String,Field>)i.next();
		Field field = (Field) entry.getValue();
		String columnName= (String) entry.getKey();
		Boolean columnHasMultiValues = (Boolean) multiValuedMap.get(field.getName());
		
		Integer startingAttribute=repeatingFieldMap.get(field.getName());
		
		//Integer startingAttribute = Integer.valueOf(repeatingFieldMap.get(field.getName()));

		if (columnName.equals("0"))
		{
			setField(field,object,itemId);//since UnyDynArray.extract(0) won't populate the primary Key

		}
		
		//column has repeating fields that occupy more than one file attribute, ex: note lines
		else if (startingAttribute !=null)
		{
			Boolean moreFields=true;
			
			Integer objectIterator=startingAttribute;//iterate over all the attributes
			while (moreFields)
			{
				String attributeContents= getEntireField(record,objectIterator.toString());
				
				if (attributeContents==null || attributeContents.isEmpty())
				{
					moreFields=false;
				}
				
				else
				{
					T nextObject = destinationObjectType.getDeclaredConstructor(Integer.class,Integer.class,String.class).newInstance(Integer.valueOf(itemId),objectIterator-startingAttribute+1,attributeContents);//constructor for repeating object must accept itemId, sequence, string
					list.add(nextObject);
					objectIterator++;
				}
			}
		}
	
		//column has from zero to many values (return an array of objects), ex: payments 
		else if (columnHasMultiValues !=null && true==columnHasMultiValues)
		{
			Boolean moreFields=true;
			int objectIterator=0;//iterate over all the positions in the field			
			while (moreFields)
			{
				int valueNumber=objectIterator+1;
				String pickFieldNumber=columnName+"."+valueNumber;
				String valueContents=getValue(record,pickFieldNumber);
				
				if ((valueContents==null || valueContents.isEmpty()) && valueNumber >= list.size())//reached the end of the values
				{
					moreFields=false;
				}else if (valueContents==null || valueContents.isEmpty()) {
					
				}
				else
				{
					
					T nextObject=null;
					if (list.size()<objectIterator+1)//when we evaluate the first field, we're going to see how many attributes exist on the pick record, and create enough objects
					{
						nextObject=sourceObjectType.newInstance();						
						list.add(objectIterator, nextObject);
					}
					
					else
					{
						nextObject = list.get(objectIterator);
					}
					Object key = keyClass.newInstance();
					keyField.setAccessible(true);
					setField(pickRecordId,key,itemId);//set the pick record id on each object
					setField(valueSequence,key,String.valueOf(valueNumber));//add the value sequence
					keyField.set(nextObject, key);//set the composite key on the object					
					setField(field,nextObject,valueContents);
					
				}
				objectIterator++;
			}
		}
		
		//single value field (no dot)
		else if (columnName.split("\\.").length==1)
		{
			setField(field,object,getEntireField(record,columnName));
		}

		//multi value field, ex: 69.2
		else if (columnName.split("\\.").length==2)
		{	
			setField(field,object,getValue(record,columnName));
		}

		//multi value field with sub-values (ex: 69.2.1)
		else if (columnName.split("\\.").length==3)
		{
			setField(field,object,getSubValue(record,columnName));
		}
		

		

	}

	return list;
}


private <T> UniDynArray updateArrayFromEntity(Class<T> entityObjectType,List<Object> entities, UniDynArray array, String itemId) throws Exception
{
	return updateArrayFromEntity(entityObjectType,entityObjectType,entities,array,itemId);
}

private <T> UniDynArray updateArrayFromEntity(Class <T> sourceObjectType, Class<T> entityObjectType,List<Object> entities, UniDynArray array, String itemId) throws Exception
{
	Set<Map.Entry<String, Field>> fieldSet=null;
	TreeMap<String, Field> uvFieldMap = universeFieldMap.get(sourceObjectType.getName());
	TreeMap<String,Boolean> multiValuedMap = multiValuedMapMap.get(sourceObjectType.getName());
	TreeMap<String,Field> sqlFieldMap = entityFieldMap.get(sourceObjectType.getName());
	TreeMap<String,Integer> repeatingFieldMap = repeatingMapMap.get(sourceObjectType.getName());
	Field compositeKey=null;//for multi-valued fields, a composite key is used consisting of the record id of the source record, combined with the value position
	Field pickRecordId=null;
	Field valueSequence=null;
	
	
	if (uvFieldMap == null)
	{
		uvFieldMap = new TreeMap<String,Field>();
		universeFieldMap.put(sourceObjectType.getName(), uvFieldMap);
	}
	
	if (sqlFieldMap == null)
	{
		sqlFieldMap = new TreeMap<String,Field>();
		entityFieldMap.put(sourceObjectType.getName(),sqlFieldMap);
	}
	
	if (multiValuedMap==null)
	{
		multiValuedMap = new TreeMap<String,Boolean>();
		multiValuedMapMap.put(sourceObjectType.getName(), multiValuedMap);
	}
	
	
	if (repeatingFieldMap==null)
	{
		repeatingFieldMap = new TreeMap<String,Integer>();
		repeatingMapMap.put(sourceObjectType.getName(), repeatingFieldMap);
	}
					
	if (uvFieldMap.isEmpty())
	{
		for (Field field:sourceObjectType.getDeclaredFields())
		{

			Boolean hasGeneratedValue = false;
			Boolean isId = false;
			
			sqlFieldMap.put(field.getName(), field);
			
			
			for (Annotation annotation:field.getAnnotations())
			{
				if (annotation instanceof EmbeddedId)
				{
					compositeKey=field;
					for (Field compositeField:field.getType().getDeclaredFields())//iterate over the primary key object
					{
						compositeField.setAccessible(true);
						
						if (compositeField.getName().contains("sequence"))
						{
							valueSequence=field;							
						}		
											
					}
					
					compositeKeyMap.put(sourceObjectType.getName(), field);//this is the field we must instantiate to create the composite
				}
				
				if (annotation instanceof UniverseColumn)
				{
					field.setAccessible(true);
					uvFieldMap.put((((UniverseColumn) annotation).name()), field);
				}
				
				else if (annotation instanceof Repeating)
				{
					repeatingFieldMap.put(field.getName(),Integer.valueOf(((Repeating)annotation).firstAttribute()));
				}
				
				else if (annotation instanceof MultiValuedColumn)
				{
					multiValuedMap.put(field.getName(), true);
				}
				
				else if (annotation instanceof GeneratedValue)
				{
					hasGeneratedValue=true;
				}
				
				else if (annotation instanceof Id)
				{
					isId=true;
				}
			}
			

		}
		

		pickRecordId= uvFieldMap.get("0");
		if (pickRecordId==null && valueSequence==null)
		{
			throw new Exception("UniverseEntityManager.createEntity failed because "+sourceObjectType.getName()+" is missing a record Id. Try adding a column with @UniverseColumn(name=\"0\") annotation.");
		}
		
		if (!multiValuedMap.isEmpty() && valueSequence==null)
		{
			throw new Exception("UniverseEntityManager.createEntity failed because "+sourceObjectType.getName()+" has multi-valued fields but it's missing a sequence. Try adding a column with @Embedded annotation.");
		}		
		
	}
	
		
	fieldSet = uvFieldMap.entrySet();			

	Class<?> keyClass = null;
	Field keyField = null;

	
	if(!compositeKeyMap.isEmpty())
	{
		keyField = compositeKeyMap.get(sourceObjectType.getName());
		keyClass = compositeKeyMap.get(sourceObjectType.getName()).getType();	
	}	
	
	if (keyField!=null)//multi-valued record 
	{


		for (Field field:keyClass.getDeclaredFields())
		{
			for (Annotation annotation:field.getAnnotations())
			{
				if (annotation instanceof UniverseColumn)
				{
					UniverseColumn column = (UniverseColumn) annotation;
					if("0".equals(column.name()))//record id
					{
						pickRecordId=field;
						field.setAccessible(true);						

					}
				}
			}

			if (field.getName().contains("sequence"))//attribute position
			{
				field.setAccessible(true);
				valueSequence=field;
			}
		}

	}		
	
	Iterator<Map.Entry<String,Field>> i = fieldSet.iterator();
	
	
	while (i.hasNext())
	{
		Map.Entry<String,Field> entry = (Map.Entry<String,Field>)i.next();
		Field field = (Field) entry.getValue();
		String columnName= (String) entry.getKey();
		Boolean columnHasMultiValues = (Boolean) multiValuedMap.get(field.getName());
		
		Integer startingAttribute=repeatingFieldMap.get(field.getName());		
		
		//column has repeating fields that occupy more than one file attribute, ex: note lines
		if (startingAttribute !=null)
		{
			for (Object entity:entities)
			{
				UniString thisElement = new UniString(entity.toString());
				array.insert(-1, thisElement);				
			}
		}
	
		//column has from zero to many values (return an array of objects), ex: payments 
		else if (columnHasMultiValues !=null && true==columnHasMultiValues)
		{
			for (Object entity:entities)
			{

					Object entityKey = keyField.get(entity);
					Integer attributeNumber= valueSequence.getInt(entityKey);
					Integer multiValue = Integer.valueOf(columnName);
					array.replace(attributeNumber,multiValue,new UniString(String.valueOf(field.get(entity))));				
				
			}
		}
		
		//single value field (no dot)
		else if (columnName.split("\\.").length==1)
		{
			for (Object entity:entities)
			{
				array.replace(Integer.valueOf(columnName),getField(field,entity));
			}
		}

		//multi value field, ex: 69.2
		else if (columnName.split("\\.").length==2)
		{	
			for (Object entity:entities)
			{
				array.replace(Integer.valueOf(columnName.split("\\.")[0]),Integer.valueOf(columnName.split("\\.")[1]),getField(field,entity));
			}
		}

		//multi value field with sub-values (ex: 69.2.1)
		else if (columnName.split("\\.").length==3)
		{
			for (Object entity:entities)
			{
				array.replace(Integer.valueOf(columnName.split("\\.")[0]),Integer.valueOf(columnName.split("\\.")[1]),Integer.valueOf(columnName.split("\\.")[2]),getField(field,entity));
			}
		}
		

		

	}

	return array;
}

private  UniString getField(Field field, Object object) throws Exception
{
	if (field.get(object)==null)
	{
		return new UniString("");//in PICK null and blank are synonymous
	}

	if (field.getType().equals(String.class))
	{
		return new UniString((String)field.get(object));
	}
	
	else if (field.getType().equals(BigDecimal.class))
	{
		return new UniString(String.valueOf((BigDecimal)field.get(object)));
	}
	
	else if (field.getType().equals(Float.class))
	{
		return new UniString(String.valueOf((Float)field.get(object)));
	}
	
	else if (field.getType().equals(Date.class))
	{
		Long internalDate = TimeDateConverter.internalDate((Date)field.get(object));
		return new UniString(String.valueOf(internalDate));
	}
	
	else if (field.getType().equals(Integer.class))
	{
		return new UniString(String.valueOf((Integer)field.get(object)));
	}
	
	else if (field.getType().equals(Long.class))
	{
		return new UniString(String.valueOf((Long)field.get(object)));
	}
	
	else
	{
		throw new Exception("UniverseEntityManager Cannot convert "+field.getType());
	}
	
}

private void setField(Field field,Object object,String string) throws Exception
{

	if (field.getType().equals(String.class))
	{
		field.set(object,string);
	}
	
	else if (field.getType().equals(BigDecimal.class))
	{
		field.set(object, !string.equals("")? extractBigDecimal(string) : new BigDecimal("0"));
	}
	
	else if (field.getType().equals(Float.class))
	{
		field.set(object, !string.equals("") ? extractFloat(string) : 0F);
	}
	
	else if (field.getType().equals(Date.class))
	{
		field.set(object, !string.equals("") ? TimeDateConverter.externalDate(string) : null);
	}
	
	else if (field.getType().equals(Integer.class))
	{
		field.set(object,!string.equals("") ? extractInteger(string) : 0);
	}
	
	else if (field.getType().equals(Long.class))
	{
		field.set(object,! string.equals("") ? Long.valueOf(string) : 0L);
	}
	
}

public Boolean testSession(){
	Boolean succeeded=true;
	try
	{
		uniSession.oconv("0", "D2-");
	}
	catch (Exception e)
	{
		succeeded=false;
	}
	
	return succeeded;
}

public static String getEntireField(UniDynArray record, String field)
{
	Integer fieldNumber=Integer.valueOf(field.split("\\.")[0]);

	return record.extract(fieldNumber).toString();
	
}


	public UniDynArray getEntireRecordFromJSON(String fileName, String recordId) throws Exception{//NOTE: this currently doesn't retrieve multi-values or sub-values

		UniDynArray array = new UniDynArray();
		String jsonString = "";

		if (SESSION_TYPE_UNIOBJECTS.equals(sessionType)) {
			UniCommand uniCommand = uniSession.command();
			uniCommand.setCommand("JSON-SERIALIZER " + fileName + " " + recordId);

// Sample command result:
//		>JSON-SERIALIZER DEBTOR 100
//		{
//			"1":"CCS100",
//				"2":"MURDOCK, SEAN",
//				"3":"10902 NE 6TH AVE",
//				"4":"",
//				"5":"",
//				"6":"",
//				"7":"",
//				"8":"",
//				"9":"",
//				"10":"",
//				"11":"123114456541123123",
//				"12":"",
//				"13":"",
//				"14":"",
//				"15":"1234.66",
//				"16":"",
//				"17":"",
//				"18":"",
//				"19":"",
//				"20":"555555555"
//		}
//		>
			uniCommand.exec();
			int status = uniCommand.status();

			if (status == UniObjectsTokens.UVS_COMPLETE) {
				jsonString = uniCommand.response().replace("\r\n", "");
			} else {
				throw new Exception("Unable to read  Record Id: " + recordId + " in File: " + fileName + " due to Unicommand status: " + status);
			}
		} else{
			String command = "cd "+ accountPath;//must be in this path so that Universe sees files local to that account (ex: DEBTOR, etc.)


			command = command+";"+uvAccountPath+"/bin/uv \"JSON-SERIALIZER " + fileName + " " + recordId+"\"";
			jsonString = executeShellCommand(command);
		}

		Type type = new TypeToken<Map<String, String>>() {
		}.getType();
		Map<String, String> fieldValues = gson.fromJson(jsonString, type);


		Set<Map.Entry<String, String>> fieldMapSet = fieldValues.entrySet();
		Integer counter = 1;
		for (Map.Entry<String, String> fieldEntry : fieldMapSet) {
			String fieldValue = fieldEntry.getValue();
			String fieldNumber = fieldEntry.getKey();
			Integer fieldInteger = Integer.valueOf(fieldNumber);
			array.replace(fieldInteger, 1, 1, fieldValue);
			counter++;
		}

		return array;
	}

	private String executeShellCommand(String command) throws Exception{
		Channel channel = remoteShellSession.openChannel("exec");
		((ChannelExec) channel).setCommand(command);
		channel.setInputStream(null);
		((ChannelExec)channel).setErrStream(System.err);
		InputStream in = channel.getInputStream();
		channel.connect();

		String response = "";
		byte[] tmp = new byte[1024];
		boolean hasMore=true;
		while(hasMore){
			while(in.available()>0 && hasMore){
				int i=in.read(tmp,0,1024);
				if(i<0) hasMore=false;
				response+=new String(tmp, 0, i);
			}
			if (channel.isClosed()){
				if (in.available()>0) continue;
				System.out.println("exit-status: "+channel.getExitStatus());
				break;
			}
			try{Thread.sleep(1000);}catch(Exception ee){}
		}

		channel.disconnect();
		return response;

	}

public static BigDecimal extractBigDecimal(String string) throws Exception
{

	String floatChars = "-.0123456789";
	
	String floatString = new String();
	
	for (char c:string.toCharArray())
	{
		if (floatChars.contains(String.valueOf(c)))
		{
			floatString+=c;
		}
	}
	
	if (floatString.length()==0)
	
	{
		throw new Exception("UniverseEntityManager.extractFloat(String) unable to convert String:"+string+" to a Float.");
	}
	
	return new BigDecimal(floatString);
}


public static Float extractFloat(String string) throws Exception
{

	String floatChars = "-.0123456789";
	
	String floatString = new String();
	
	for (char c:string.toCharArray())
	{
		if (floatChars.contains(String.valueOf(c)))
		{
			floatString+=c;
		}
	}
	
	if (floatString.length()==0)
	
	{
		throw new Exception("UniverseEntityManager.extractFloat(String) unable to convert String:"+string+" to a Float.");
	}
	
	return Float.valueOf(floatString);
}

public static Integer extractInteger(String string) throws Exception
{

	String integerChars = ".0123456789";
	
	String integerString = new String();
	
	for (char c:string.toCharArray())
	{
		if (integerChars.contains(String.valueOf(c)))
		{
			integerString+=c;
		}
	}
	
	if (integerString.length()==0)
	
	{
		throw new Exception("UniverseEntityManager.extractFloat(String) unable to convert String:"+string+" to an Integer.");
	}
	
	return Integer.valueOf(integerString);
}

public UniDynArray getRecord(String fileName, String recordId) throws UniSessionException, UniFileException
{
	
	UniFile file = getFile(fileName);

	UniDynArray array = new UniDynArray(file.read(recordId));
	return array;
	

}

public <T> void putRecord(Class <T> entityObjectType, String recordId, Object entity) throws Exception{
	Annotation annotation = entityObjectType.getAnnotation(UniverseTable.class);
	UniverseTable table = (UniverseTable) annotation;
	
	List<Object> entities = new ArrayList<Object>();
	entities.add(entity);
	putRecords(table.name(),entityObjectType,recordId,entities);
	
}


public <T> void putRecords(String fileName,Class <T> entityObjectType, String recordId, List<Object> entities) throws Exception{
	
	UniDynArray array = new UniDynArray();	
	array=updateArrayFromEntity(entityObjectType, entities, array, recordId);
	getFile(fileName).write(recordId, array);	
	
}

public String getStringFromArray(UniDynArray array) throws Exception{
	UniString uniString = new UniString();
	char[] arrayCharacters=array.toCharArray();
	String arrayString=String.valueOf(arrayCharacters);
	arrayString=arrayString.replaceAll(uniString.getMarkCharacter(UniTokens.FM), "@FM");
	arrayString=arrayString.replaceAll(uniString.getMarkCharacter(UniTokens.VM), "@VM");
	arrayString=arrayString.replaceAll(uniString.getMarkCharacter(UniTokens.SVM), "@SVM");
	//uniString=new UniString(arrayString);
	//return uniString;
	return arrayString;
}

public UniFile getFile (String fileName) throws UniSessionException, UniFileException
{
	UniFile file = fileMap.get(fileName);
	if(file==null)
	{ 
		file= getFile(fileName, uniSession);
		fileMap.put(fileName, file);
	}
	return file;
}

private static UniFile getFile(String fileName,UniSession session) throws UniSessionException, UniFileException
{
	UniFile file =session.open(fileName);	
	return file;
}

public static String getPathFromUvAccount(String hostName, String uvAccountPath, String accountName) throws Exception{
	
	UniSession uvAccountSession = getSession(hostName,uvAccountPath);
	UniFile uvAccountFile= getFile("UV.ACCOUNT",uvAccountSession);
	UniDynArray accountRecord= new UniDynArray(uvAccountFile.read(accountName));
	String requestedAccountPath= getEntireField(accountRecord,"11");
	uvAccountSession.disconnect();//since we hardly ever use this method (if we cache values) it's okay to open a new connection every time as long as we close it promptly
	return requestedAccountPath;
	
}

public static String getPathFromUdAccount(String hostName, String sysAccountPath, String accountName) throws Exception{
	
	UniSession udAccountSession = getSession(hostName,sysAccountPath);
	UniFile udAccountFile= getFile("UD.ACCOUNT",udAccountSession);
	UniDynArray accountRecord= new UniDynArray(udAccountFile.read(accountName));
	String requestedAccountPath= getEntireField(accountRecord,"1");
	udAccountSession.disconnect();//since we hardly ever use this method (if we cache values) it's okay to open a new connection every time as long as we close it promptly
	return requestedAccountPath;
	
}


public static UniSession getSession(String hostName, String accountPath) throws Exception{
	UniSession uniConnection = new UniSession();
	uniConnection.setUserName(ConfigurationUtility.getProperty("session.username"));
	String password=ConfigurationUtility.getProperty("session.encrypted.password");
	uniConnection.setPassword(password);
	uniConnection.setHostName(hostName);
	uniConnection.setAccountPath(accountPath);	
	uniConnection.setDataSourceType(ConfigurationUtility.getProperty("session.datasource.type"));
	uniConnection.connect();	
	return uniConnection;
	
}

public void closeFiles(){	
	fileMap.clear();	
}

public void reconnect() throws Exception{
	getUniSession().disconnect();
	closeFiles();//remove opened file handles connected with current uniSession so they can be reopened
	setUniSession(getSession(uniSession.getHostName(), uniSession.getAccountPath()));
}

public static String getValue (UniDynArray record, Integer field, Integer value)
{
	return record.extract(field,value).toString();
}

public static String getValue (UniDynArray record, String field)
{
	Integer fieldNumber=Integer.valueOf(field.split("\\.")[0]);
	Integer valueNumber=Integer.valueOf(field.split("\\.")[1]);
	return getValue(record,fieldNumber,valueNumber);

}

public static String getSubValue (UniDynArray record, String field)
{
	Integer fieldNumber=Integer.valueOf(field.split("\\.")[0]);
	Integer valueNumber=Integer.valueOf(field.split("\\.")[1]);
	Integer subValue=Integer.valueOf(field.split("\\.")[1]);
	return getSubValue(record,fieldNumber,valueNumber,subValue);
}

public static String getSubValue (UniDynArray record, Integer field, Integer value, Integer subValue)
{
	return record.extract(field,value,subValue).toString();
}




}

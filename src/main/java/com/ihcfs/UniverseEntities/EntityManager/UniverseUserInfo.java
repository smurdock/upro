package com.ihcfs.UniverseEntities.EntityManager;

import com.jcraft.jsch.UserInfo;

/**
 * Created by Administrator on 1/4/2017.
 */
public class UniverseUserInfo implements UserInfo {
    private String password;

    @Override
    public String getPassphrase() {
        return null;
    }

    public void setPassword(String password){
        this.password=password;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean promptPassword(String s) {
        return true;
    }

    @Override
    public boolean promptPassphrase(String s) {
        return true;
    }

    @Override
    public boolean promptYesNo(String s) {
        return true;
    }

    @Override
    public void showMessage(String s) {

    }
}

package com.ihcfs.UniverseEntities.EntityManager;
import java.util.ArrayList;
import java.util.List;

import com.ihcfs.UniverseEntities.config.ConfigurationUtility;

import asjava.uniobjects.UniSession;

public class UniverseEntityManagerFactory {
private static List<UniverseEntityManager> entityManagerPool = new ArrayList<UniverseEntityManager>();
private static Integer sessionMax;
private static Integer reTryMax;

public static UniverseEntityManager getPooledEntityManager(String hostName, String accountPath, String fileName) throws Exception{
	UniverseEntityManager availableEntityManager=null;
	Boolean done =false;

	initialize();
	
	//for (UniverseEntityManager em:entityManagerPool)
	while (!done)
	{
		List<UniverseEntityManager> availableEntityManagers = getAvailableEntityManagers();
		
		if (availableEntityManagers !=null && availableEntityManagers.size()>0)
		{
			for (UniverseEntityManager em:availableEntityManagers)
			{
				if (!em.isInUse() && accountPath.equals(em.getUniSession().getAccountPath()))
				{
					availableEntityManager=em;
					availableEntityManager.setInUse(true);
					done=true;
				}
			}
		}
		
		if (!done && entityManagerPool.size()<sessionMax)
		{
			UniSession session = UniverseEntityManager.getSession(hostName,accountPath);
			UniverseEntityManager newEntityManager= new UniverseEntityManager(session);
			entityManagerPool.add(newEntityManager);
			availableEntityManager=entityManagerPool.get(entityManagerPool.size()-1);
			availableEntityManager.setInUse(true);
			done=true;			
		}
		
		if (!done && availableEntityManagers!=null && availableEntityManagers.size()>0)
		
		{
			for (UniverseEntityManager em:availableEntityManagers)
			{	
				if (!em.isInUse() && !accountPath.equals(em.getUniSession().getAccountPath()))
				{
					availableEntityManager=em;
					availableEntityManager.setInUse(true);
					done=true;
				}
			}
		}
		
		if(done)
		{
			Boolean sessionActive= availableEntityManager.testSession();
			if (!sessionActive)//make sure the connection wasn't dropped, if it was try to renew it
			{
				availableEntityManager.setUniSession(UniverseEntityManager.getSession(hostName,accountPath));
			}

		}
		
		else{
			sessionMax = Integer.valueOf(ConfigurationUtility.getProperty("session.count.limit"));//update this in case the value was changed (we're waiting any way)
			reTryMax = Integer.valueOf(ConfigurationUtility.getProperty("entity.manager.retry.max","1000"));//update this in case the value was changed (we're waiting any way)
			Thread.sleep(1000);//Sleep for a second
		}
	}
	
	return availableEntityManager;
	
}

private static List<UniverseEntityManager> getAvailableEntityManagers(){
	List<UniverseEntityManager> availableEntityManagers = new ArrayList<UniverseEntityManager>();
	for (UniverseEntityManager em:entityManagerPool)
	{
		if (!em.isInUse())
		{
			availableEntityManagers.add(em);
		}
	}
	return availableEntityManagers;
}

public void closeEntityManagerPool() throws Exception{
	for (UniverseEntityManager em:entityManagerPool)
	{
		em.close();
	}
}

private static void initialize() throws Exception{
	if (sessionMax==null)
	{
		sessionMax = Integer.valueOf(ConfigurationUtility.getProperty("session.count.limit","1"));
	}
	
	if (reTryMax==null)
	{
		reTryMax = Integer.valueOf(ConfigurationUtility.getProperty("entity.manager.retry.max","1000"));
	}

	
}
}

/*
* Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (�Intermountain�) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * � 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 



package com.ihcfs.UniverseEntities.conversion;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;

import jcifs.smb.SmbFileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asjava.uniobjects.UniSelectList;
import asjava.uniobjects.UniSession;

import com.ihcfs.UniverseEntities.Data.DebtorDO;
import com.ihcfs.UniverseEntities.Data.CarrierDO;
import com.ihcfs.UniverseEntities.Data.ETLJobDO;
import com.ihcfs.UniverseEntities.Data.ForwardClientDO;
import com.ihcfs.UniverseEntities.Data.NoteDO;
import com.ihcfs.UniverseEntities.Data.PayPlanDO;
import com.ihcfs.UniverseEntities.Data.PaymentHistoryDO;
import com.ihcfs.UniverseEntities.Data.RpPacketDO;
import com.ihcfs.UniverseEntities.Data.UniverseExtractionType;
import com.ihcfs.UniverseEntities.Data.UniverseTable;
import com.ihcfs.UniverseEntities.EntityManager.EMFactory;
import com.ihcfs.UniverseEntities.EntityManager.UniverseEntityManager;
import com.ihcfs.UniverseEntities.PickUtilities.TimeDateConverter;
import com.ihcfs.UniverseEntities.config.ConfigurationUtility;


/**
 * 
 * @author smurdoc2
 * 
 * This Service is responsible for ETL (Extract, Transform, Load) operations from The Managemed/Universe Database to the DB2 Database
 *
 */

public class ETLService implements Runnable{
	
	EntityManager jpaEntityManager;
	UniverseEntityManager u2EntityManager;
	private Integer alreadyLoaded;
	private String fileName;
	private Boolean isFinished=false;

	private String listName;
	private Integer loadMax;
	private String startRecord;
	private Class entityExtractionClass;
	private Class entityPersistenceClass;
	private ETLJobDO etlJob;
	private Integer commitFrequency;
	private Boolean stopRequested=false;
	private String lastInitials;
	private Date lastDate;
	private Boolean isMergeRecordsActive=false;
	private Boolean isSambaListReadActive=false;
	private UniSelectList fileSelect=null;
	SmbFileInputStream sambaSelect;
	BufferedReader bufferedSelect;	
	String recordId=null;
	Logger logger;
	
	public ETLService () throws Exception
	{
		logger = LoggerFactory.getLogger("export");
		
		try{
			jpaEntityManager = EMFactory.createEntityManager();
		}
		
		catch (Exception e){
			logger.error("ETLService",e);
		}
		alreadyLoaded=0;	
		commitFrequency=Integer.valueOf(ConfigurationUtility.getProperty("commit.frequency"));		
	}
	
	public void run(){

		logger = LoggerFactory.getLogger("export");		
		
		try {
			extract();
		} 
	
		
		catch (Exception e) {
			
			logger.error("ETLService", e);
			setStopRequested(true);
			jpaEntityManager.close();
			isFinished=true;			
		}
	}
	
	public void extract() throws Exception
	{
		if(listName==null)
		{
			throw new Exception ("listName has not been set");
		}

		if (loadMax==null)
		{
			throw new Exception ("loadMax has not been set");
		}

		if(fileName==null)
		{
			throw new Exception ("fileName has not been set");
		}		

		UniSession session = new UniSession();
		session.setUserName(ConfigurationUtility.getProperty("session.username"));
		String password=ConfigurationUtility.getProperty("session.encrypted.password");
		session.setPassword(password);
		session.setHostName(ConfigurationUtility.getProperty("session.hostname"));
		session.setAccountPath(ConfigurationUtility.getProperty("session.path"));	
		session.setDataSourceType(ConfigurationUtility.getProperty("session.datasource.type"));
		session.connect();

		u2EntityManager = new UniverseEntityManager(session);
		
		if (isSambaListReadActive)//read list using samba share 
		{	sambaSelect=u2EntityManager.getSelectSamba(listName);
			InputStream inputSelect = (InputStream) sambaSelect;
			InputStreamReader inputSelectReader = new InputStreamReader(inputSelect);
			bufferedSelect = new BufferedReader(inputSelectReader);
			
		}
		
		else //read list using Universe
		{
			fileSelect = u2EntityManager.getSelect(listName);
		}

		Boolean finished=false;
		Boolean skip=false;

		EntityTransaction tx = jpaEntityManager.getTransaction();
		tx.begin();	
		getETLJob(tx);
		if(isMergeRecordsActive)
		{
			etlJob.setCompleted(false);//start over at the beginning of the list
		}
		
		etlJob.setLastRunDate(new Date(System.currentTimeMillis()));

		if (etlJob.getLastId()!=null && !isMergeRecordsActive)//we have an id stored showing the last record exported
		{
			skip=true;
		}

		while (!finished)

		{
				if (isSambaListReadActive)//read list using samba share 
				{			
					recordId=bufferedSelect.readLine();
				}
				else //read list using Universe
				{
					recordId=fileSelect.next().toString();
					if (recordId.isEmpty())
					{
						recordId=null;
					}
				}				
				
				if (recordId==null)
				{
					etlJob.setCompleted(true);
				}
				
				if (recordId==null || etlJob.getLoadCount()>=loadMax || etlJob.isCompleted())
				{
					finished=true;
				}
				
				else if (skip)
				{
					if (recordId.equals(etlJob.getLastId()))
					{
						skip=false;//load records after this one
					}
				}				
				else
				{
					try
					{
						logger.info("Extracting "+fileName+":"+recordId);
						List recordList = u2EntityManager.find(entityExtractionClass,entityPersistenceClass,recordId);//Universe record
						if (recordList==null)
						{
							throw new Exception("Could not read "+fileName+" record:"+recordId);					
						}


						else
						{	

							for (int i=0;i<recordList.size();i++)
							{
								Object record = recordList.get(i);
								Boolean loaded=false;
								Boolean isValid=transform(record);

								if (isValid)
								{
									loaded=load(record);
								}
								
								if(loaded)
								{
									etlJob.setLoadCount(etlJob.getLoadCount()+1);//actual writes (including multi-values)
									
									if (i==recordList.size()-1)//Universe Record Count (excluding multi-values)
									{
										etlJob.setExtractCount(etlJob.getExtractCount()+1);
									}
								}								

								updateETLJob(recordId);

								if (etlJob.getLoadCount() %commitFrequency==0 && etlJob.getLoadCount()>0)//commit based on properties file "commit.frequency" setting
								{
									tx = commit(tx);

									if (stopRequested.equals(true))
									{
										finished=true;
									}
								}
							}
							
							

						}					
					}
					
					
					catch (asjava.uniobjects.UniFileException ue){
						logger.error("ETLService", ue);
						if(ue.getMessage().contains("The RPC failed"))//server disconnected
						{
							logger.info("Export Process became disconnected from the server");
							throw ue;//process should stop, due to no active connection to the Universe DB
						}
						else//record not found
						{
							logger.info("Export Process could not find "+fileName+" record: "+recordId+" from savelist: "+listName);
						}
					}

				}


			}

		if (isSambaListReadActive)//read list using samba share 
		{
			sambaSelect.close();
		}
		
		else{//read list using Universe
			u2EntityManager.closeSelect(listName);
		}
		
		etlJob.setStopDate(new Date(System.currentTimeMillis()));
		updateETLJob(recordId);
		tx.commit();	
		u2EntityManager.close();
		session.disconnect();	
		jpaEntityManager.close();
		isFinished=true;

	}
	
	private EntityTransaction commit(EntityTransaction tx) throws Exception{
		jpaEntityManager.merge(etlJob);
		tx.commit();
		tx = jpaEntityManager.getTransaction();
		tx.begin();	
		return tx;
	}
	
	private void updateETLJob(String recordId){
		
		
		etlJob.setPingTime(new Date(System.currentTimeMillis()));
		if (recordId!=null)
		{
			etlJob.setLastId(recordId);			
		}
	}
	
	
	private ETLJobDO getETLJob(EntityTransaction tx){
		
		Query jobQuery= jpaEntityManager.createQuery("select j from PHPMETLJOB j where j.fileName ='"+fileName+"' and j.listName='"+listName+"'" );
		
		try
		{
			etlJob = (ETLJobDO) jobQuery.getSingleResult();		
		}
		
		catch (javax.persistence.NoResultException e)
		{
			etlJob=null;
		}
		
		if (etlJob ==null)
		{
			etlJob = new ETLJobDO();
			
			etlJob.setCompleted(false);
			etlJob.setFileName(fileName);
			etlJob.setListName(listName);
			etlJob.setLastRunDate(new Date(System.currentTimeMillis()));
			etlJob.setFirstRunDate(new Date(System.currentTimeMillis()));
			etlJob.setLoadCount(0);
			etlJob.setExtractCount(0);
			etlJob.setPingTime(new Date(System.currentTimeMillis()));
			
			try	
			
			{

				jpaEntityManager.persist(etlJob);
				//commit(tx);
			}
			
			catch (Exception e)
			{

				System.out.println(e.getMessage());
			}	
			

		}
		return etlJob;
	}
	
	public synchronized Boolean stopRequested()
	{
		return stopRequested;
	}
	
	public synchronized void setStopRequested(Boolean stopRequested)
	{
		this.stopRequested=stopRequested;
	}
	
	private Boolean transform (Object object)
	{
		Boolean isValid=true;	
		
		logger.info("Transforming "+fileName+":"+recordId);
		if (entityExtractionClass.getName().contains("AccountDO"))
		{
			DebtorDO account =(DebtorDO) object;
			//logger.info("Transforming DEBTOR:"+account.getDebtorNumber());
			transform((DebtorDO) object);

		}

		else if (entityExtractionClass.getName().contains("CarrierDO"))
		{
			CarrierDO carrier=(CarrierDO) object;
			//logger.info("Transforming CARRIER:"+carrier.getCarrierId());
			carrier.setJobId(etlJob.getUniqueId());
		}
			
//		else if (entityExtractionClass.getName().contains("FiscalDO"))
//		{
//			//logger.info("Transforming FISCAL:"+((FiscalDO)object).getDebtorNumber());
//			transform ((FiscalDO) object);
//		}
		
		else if (entityExtractionClass.getName().contains("ForwardClientDO"))
		{
			ForwardClientDO forwardClient = (ForwardClientDO) object;
			//logger.info("Transforming FORWARD-CLIENT:"+forwardClient.getForwardClientId());
			forwardClient.setJobId(etlJob.getUniqueId());			
		}
		
		else if (entityExtractionClass.getName().contains("MemoDO"))
		{
			if (object instanceof NoteDO)//the list of objects will include a MemoDO object in the first element
			{
				NoteDO noteRecord = (NoteDO) object;	
				//logger.info("Transforming MEMO:"+noteRecord.getNotePK().getDebtorNumber()+" sequence:"+noteRecord.getNotePK().getSequence());
				transform(noteRecord);
			}
		}
		
		else if (entityExtractionClass.getName().contains("PaymentHistoryDO"))
		{
			PaymentHistoryDO paymentHistory = (PaymentHistoryDO) object;
			//logger.info("Transforming PAYMENT-HISTORY:"+paymentHistory.getPaymentHistoryPK().getDebtorNumber()+" sequence:"+paymentHistory.getPaymentHistoryPK().getSequence());
			paymentHistory.setJobId(etlJob.getUniqueId());			
		}
				
		else if (entityExtractionClass.getName().contains("PayPlanDO"))
		{
			PayPlanDO payPlan = (PayPlanDO) object;
			//logger.info("Transforming PAYPLAN:"+payPlan.getPayPlanPK().getPayPlanId()+" sequence:"+payPlan.getPayPlanPK().getSequence());
			transform(payPlan);		
		}
		
		else if (entityExtractionClass.getName().contains("RpPacketDO"))
		{
			RpPacketDO rpPacket = (RpPacketDO) object;
			//logger.info("Transforming RP-PACKET:"+rpPacket.getRPPacketPK().getPacketId()+" sequence:"+rpPacket.getRPPacketPK().getSequence());
			rpPacket.setJobId(etlJob.getUniqueId());
			
		}
		
		
		return isValid;
	}
	
	private Boolean transform (NoteDO noteRecord)
	{
		noteRecord.setJobId(etlJob.getUniqueId());
		Boolean isValid=true;
		Boolean isActionCode=false;
		Boolean isFreeForm=false;
		
		if (noteRecord.getNoteLine().startsWith("*"))
		{
			noteRecord.setNoteDate(lastDate);
			noteRecord.setAgentInitials(lastInitials);
			noteRecord.setNoteLine(noteRecord.getNoteLine().replaceFirst("\\*", ""));
		}
		
		else
		{
			if(noteRecord.getNoteLine().startsWith("&"))//free form note
			{
				//noteRecord.setNoteLine(noteRecord.getNoteLine().replaceFirst("&", ""));
				isFreeForm=true;
			}
			
			else if (noteRecord.getNoteLine().startsWith("#"))//action code notes
			{
				//noteRecord.setNoteLine(noteRecord.getNoteLine().replaceFirst("#", ""));
				isActionCode=true;
			}
			
			else if (noteRecord.getNoteLine().startsWith("^") || noteRecord.getNoteLine().startsWith("!") || noteRecord.getNoteLine().startsWith("%") || noteRecord.getNoteLine().startsWith("@"))//system generated
			{
				noteRecord.setSystemNote(true);							
			}			
			
			char firstChar=noteRecord.getNoteLine().toCharArray()[0];
			
			String replaceChars="";
			if (firstChar=='^' || firstChar=='&')
			{
				replaceChars="\\"+firstChar;//since ^ or & can be part of a regular expression, need to escape these
			}
			
			else
			{
				replaceChars=String.valueOf(firstChar);
			}
			
			String newNoteLine=noteRecord.getNoteLine().replaceFirst(replaceChars, "");
			
			noteRecord.setNoteLine(newNoteLine);//drop the first character of the note line
			
			String[] pipeDelimitedNotes = noteRecord.getNoteLine().split("\\|");
			
			if ((isActionCode || isFreeForm) && pipeDelimitedNotes.length>1)//agent initials come after the pipe ("|") symbol
			{
				String agentInitials=pipeDelimitedNotes[1];
				if (agentInitials.length()<=3)//agent initials are up to three characters
				{
					noteRecord.setAgentInitials(agentInitials.toUpperCase());
				}
			}
			
			
			String[] notes = noteRecord.getNoteLine().split(" ");
			String noteDay= notes[1];
			String noteTime=notes[0];
			String actionCode=notes[2];
			
			if (isActionCode && actionCode.length() >0 && actionCode.length()<=3)//action code is three digits or less
			{
				noteRecord.setActionCode(Integer.valueOf(actionCode));
			}
			
			noteRecord.setNoteLine(noteRecord.getNoteLine().replaceFirst(noteDay, ""));//remove internal date from noteline
			noteRecord.setNoteLine(noteRecord.getNoteLine().replaceFirst(noteTime,""));//remove internal time from noteline
			noteRecord.setNoteLine(noteRecord.getNoteLine().trim());//remove leading spaces
			
			
			try{
				Date noteDate= TimeDateConverter.externalDate(noteDay);//midnight on the date of the note
				Long noteSeconds =Long.valueOf(noteTime);//note time in seconds past midnight MST
				Long noteMilliSeconds=noteSeconds*1000;//note time in milliseconds past midnight MST
				Long milliSecondsDate=noteDate.getTime();//note time in milliseconds past midnight GMT January 1 1970

				milliSecondsDate=milliSecondsDate+noteMilliSeconds;
				noteRecord.setNoteDate(new Date(milliSecondsDate));
				lastDate=noteRecord.getNoteDate();
				lastInitials=noteRecord.getAgentInitials();
			}
			
			catch (Exception e)
			{
				lastDate=null;
				lastInitials=null;
				String error = "ETLService could not convert Date and Time for memo record: "+recordId+" note line: "+noteRecord.getNotePK().getSequence();
				logger.error(error);
				logger.error("ETLService", e);
				
			}
			
		}
		
		return isValid;
	}
	
	private Boolean transform (PayPlanDO payplanRecord)
	{
		payplanRecord.setJobId(etlJob.getUniqueId());	
		payplanRecord.setJobId((etlJob.getUniqueId()));
		Boolean isValid=true;
		if (payplanRecord.getAddedToPlanDate()==null || payplanRecord.getAccountId()==null)
		{
			isValid=false;
		}
		
		return isValid;
	}
	
//	private void transform (FiscalDO fiscalRecord)
//	{
//		fiscalRecord.setJobId(etlJob.getUniqueId());
//		if (fiscalRecord.getCbrInfo()!=null && fiscalRecord.getCbrInfo().length()!=0)
//		{
//			String cbrData[]= fiscalRecord.getCbrInfo().split("\\;");
//			
//			if (cbrData.length > 0)
//			{
//				if (cbrData[0].contains("-"))
//				{							
//				SimpleDateFormat dateFormat = new SimpleDateFormat ("MM-dd-yy");
//				try {
//					
//					fiscalRecord.setCbrReportDate(dateFormat.parse(cbrData[0]));
//					
//				} catch (ParseException e) {
//					System.out.println("Unable to convert CBR Report Date for Account:"+fiscalRecord.getDebtorNumber());
//				}
//				}
//				
//				else
//				{
//					fiscalRecord.setCbrReportDate(TimeDateConverter.externalDate(cbrData[0]));
//				}
//			}
//			
//			if (cbrData.length>1)
//			{
//				fiscalRecord.setCbrReportType(cbrData[1]);
//			}
//			
//			if (cbrData.length>2)
//			{
//				try{
//				fiscalRecord.setCbrReportAmount(new BigDecimal(cbrData[2]));
//				}
//				
//				catch (Exception e){
//					System.out.println("Unable to convert CBR Report Amount for Account:"+fiscalRecord.getDebtorNumber());
//				}
//			}
//			
//		}
//		
//		if (fiscalRecord.getCurrFinancialClass()!=null)
//		{
//			fiscalRecord.setCarrierId(fiscalRecord.getCurrFinancialClass()+"_DEF");
//		}
//		
//		else if (fiscalRecord.getOriginalFinancialClass()!=null)
//		{
//			fiscalRecord.setCarrierId(String.valueOf(fiscalRecord.getOriginalFinancialClass())+"_DEF");
//		}
//	}
//	
	private void transform(DebtorDO account)
	{
		account.setJobId(etlJob.getUniqueId());
		if (account.getPacketId()!=null && account.getPacketId().length()!=0)
		{
			String packetId= account.getPacketId().split("\\*")[0];
			account.setPacketId(packetId);			
		}
		
		if (account.getGuarantorPersonKey()!=null && account.getGuarantorPersonKey().length()!=0)
		{
			String personKey = account.getGuarantorPersonKey().split("\\:")[1];
			account.setGuarantorPersonKey(personKey);
		}
		

		account.setSelfPay(account.getSelfPay());
		account.setInsurance(account.getInsurance());
		account.setAssignedPrincipalOwing(account.getAssignedPrincipalOwing());
		account.setAssignedPrincipalReceived(account.getAssignedPrincipalReceived());
		account.setNonAssignMisc1Owing(account.getNonAssignMisc1Owing());
		account.setNonAssignMisc1Received(account.getNonAssignMisc1Received());
		account.setTotalBalance(account.getTotalBalance());
	}

	
	private Boolean load (Object object)
	{
		Boolean written=true;
		
		logger.info("Loading "+fileName+":"+recordId);
		
		try	
		
		{
			if(isMergeRecordsActive)
			{
				jpaEntityManager.merge(entityPersistenceClass.cast(object));//update (will insert if not found)
			}
			
			else
			{
				jpaEntityManager.persist(entityPersistenceClass.cast(object));//insert (will throw duplicate key exception if found)
			}
		}
		
		catch (Exception e)
		{
			written = false;
			System.out.println(e.getMessage());
		}
			
		return written;
		
	}
	
	public Integer getAlreadyLoaded() {
		return alreadyLoaded;
	}

	public void setAlreadyLoaded(Integer alreadyLoaded) {
		this.alreadyLoaded = alreadyLoaded;
	}

	public ETLJobDO getEtlJob() {
		return etlJob;
	}
	
	public Integer getLoadCount(){
		if (etlJob==null)
		{
			return 0;
		}
		else
		{
			return etlJob.getLoadCount();
		}
		
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
		

		Iterator modelIterator = jpaEntityManager.getMetamodel().getEntities().iterator();
		
		while(modelIterator.hasNext())
		{
			EntityType<?> entity= (EntityType<?>) modelIterator.next();
			String fileNameWithoutDash = fileName.replaceAll("-", "");
			fileNameWithoutDash = fileNameWithoutDash.replaceAll("\\.", "");
			Class<?> klass =entity.getJavaType();
			if (fileNameWithoutDash.equals(entity.getName()))
			{
				entityPersistenceClass=klass;
			}
			
			for (int i=0; i < klass.getAnnotations().length; i ++)
			{
				Annotation annotation = klass.getAnnotations()[i];
				if (annotation instanceof UniverseTable )
				{
					UniverseTable tableAnnotation = (UniverseTable) annotation;
					if (tableAnnotation.name().equals(fileName))
					{
						entityExtractionClass=klass;
					} 
				} else if (annotation instanceof UniverseExtractionType) {
					UniverseExtractionType persistenceTypeAnnotation = (UniverseExtractionType) annotation;
					if (persistenceTypeAnnotation.name().equals(fileName))
					{
						entityPersistenceClass=klass;
					} 
					
				}
			}
		}
		
		if (entityExtractionClass==null)

		{
			String className= fileName.toLowerCase();
			String firstLetterLowerCase=String.valueOf(className.charAt(0));
			String firstLetterUpperCase=firstLetterLowerCase.toUpperCase();
			className=className.replaceFirst(firstLetterLowerCase, firstLetterUpperCase);
			className=className+"DO";
			className="com.ihcfs.MMed.Data."+className;
			
			if (entityExtractionClass==null)
			{
			try {
				entityExtractionClass=Class.forName(className);
				
			} catch (Exception e) {
				System.out.println("No class mapped for "+fileName+" try adding the class to the persistence.xml.");
				e.printStackTrace();				
			}}
		}
		
		if (entityPersistenceClass==null)//if nothing is mapped separately (ex: one to many) then just assume it's a one-to-one
		{
			entityPersistenceClass=entityExtractionClass;
		}
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public Boolean getIsSambaListReadActive(){//use smb to read lists (overcomes 11 concurrent list limit with UniObjects)
		return isSambaListReadActive;
	}
	
	public void setIsSambaListReadActive(Boolean isSambaListReadActive){
		this.isSambaListReadActive=isSambaListReadActive;
	}
	
	public Boolean getIsMergeRecordsActive(){//update existing records in destination system
		return isMergeRecordsActive;
	}
	
	public void setIsMergeRecordsActive(Boolean isMergeRecordsActive){
		this.isMergeRecordsActive=isMergeRecordsActive;
	}	
	
	public Integer getLoadMax() {
		return loadMax;
	}

	public void setLoadMax(Integer loadMax) {
		this.loadMax = loadMax;
	}

	public String getStartRecord() {
		return startRecord;
	}

	public void setStartRecord(String startRecord) {
		this.startRecord = startRecord;
	}
	
	public synchronized Boolean isFinished(){
		return isFinished;
	}
	
	public synchronized void setFinished(Boolean finished){
		this.isFinished=finished;
	}
	
	
}

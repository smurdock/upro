/*
 * Confidential
 * These computer programs and materials and the content and information contained in these computer programs and accompanying materials are confidential and proprietary to Intermountain Healthcare (“Intermountain”) and may not be disclosed or used outside of Intermountain without the written consent of Intermountain in each case.
 *
 * Unpublished Work of Authorship
 * © 2012-2013 Intermountain Healthcare.
 * All Rights Reserved
 *
 *  No License
 *  No license or right to any of these computer programs or materials or the content or information of these computer programs and materials is granted, unless and except to the extent of a formal written agreement with Intermountain Healthcare.*/ 


package com.ihcfs.UniverseEntities.conversion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import com.ihcfs.UniverseEntities.EntityManager.UniverseEntityManager;
import com.ihcfs.UniverseEntities.config.ConfigurationUtility;
import com.ihcfs.UniverseEntities.conversion.ETLService;

import asjava.uniclientlibs.UniString;
import asjava.uniobjects.UniSelectList;
import asjava.uniobjects.UniSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ExtractionTool  {
	
static Map<String,String> configuredLists;	
static List<ETLService> exportJobList;
static List<Thread> threadList;
static UniverseEntityManager em;
static UniSession uniConnection;

static Integer loadMax=null;
	
public static void main(String[] args)
{
	Logger logger = LoggerFactory.getLogger("export");
	configuredLists = new HashMap<String,String>();
	exportJobList = new CopyOnWriteArrayList<ETLService>();
	threadList = new CopyOnWriteArrayList<Thread>();
	
	try
	{
		logger.info("Export starting: "+new Date(System.currentTimeMillis()).toString());
		start(logger);

	}
	
	catch (Exception e)
	{
		logger.error("Export couldn't complete", e);
	}
}

public static Boolean isTimeToStop() throws Exception
{
	Boolean stop = false;
	String autoStopTime=ConfigurationUtility.getProperty("auto.stop.time");
	SimpleDateFormat dateFormat = new SimpleDateFormat ("MM-dd-yy");
	SimpleDateFormat hhMMDateFormat= new SimpleDateFormat("MM-dd-yy HH:mm");
	Date currentTime = new Date(System.currentTimeMillis());
	String todayString = dateFormat.format(currentTime);
	String stopTimeFormatted=todayString+" "+autoStopTime;
	Date stopTime=hhMMDateFormat.parse(stopTimeFormatted);//time today when job must stop
	if (stopTime.before(currentTime))
	{
		stop=true;
	}	
	return stop;
}


public static void start(Logger logger) throws Exception
{
	uniConnection = new UniSession();
	uniConnection.setUserName(ConfigurationUtility.getProperty("session.username"));
	String password=ConfigurationUtility.getProperty("session.encrypted.password");
	uniConnection.setPassword(password);
	uniConnection.setHostName(ConfigurationUtility.getProperty("session.hostname"));
	uniConnection.setAccountPath(ConfigurationUtility.getProperty("session.path"));	
	uniConnection.setDataSourceType(ConfigurationUtility.getProperty("session.datasource.type"));
	uniConnection.connect();

	em = new UniverseEntityManager(uniConnection);
	//UniSelectList fileSelect = em.getSelect(listName);	
	

	
	String propertyName="session.count.limit";//Universe Session Limit
	String propertyVal=ConfigurationUtility.getProperty(propertyName);
	if (propertyVal==null)
	{
		throw new Exception("Export jobs did not start because "+propertyName+" has not been set.");
	}
	
	Integer sessionCountLimit=Integer.valueOf(propertyVal);

	if (isTimeToStop())
	{
		throw new Exception ("Export jobs did not start because auto.stop.time is before the current time.");
	}
	
	Boolean moreJobs=true;
	Integer jobCursor = 1;
	while (moreJobs && jobCursor<=sessionCountLimit)
	{
		try
		{
			logger.info("Export trying list: "+jobCursor+" : "+new Date(System.currentTimeMillis()).toString());
			moreJobs=getNextJobStarted(jobCursor);
		}
		catch (Exception e)
		{
			if (e.getMessage().contains("doesn't exist or has no records."))
			{
				logger.info(e.getMessage());
			}
			
			else
			{
				throw e;
			}
		}
		jobCursor++;
	}

	
	Boolean finished=false;
	while (!finished)
	{
		Thread.sleep(2000);
		
		if ("true".equals(ConfigurationUtility.getProperty("stop.requested")))
		{
					
			logger.info("Export stop requested: "+new Date(System.currentTimeMillis()).toString());		
			for (ETLService service:exportJobList)
			{
				service.setStopRequested(true);//request service to stop
			}
		}
		
		else if (isTimeToStop())
		{

			logger.info("Export stopping at configured time: "+new Date(System.currentTimeMillis()).toString());		
			for (ETLService service:exportJobList)
			{
				service.setStopRequested(true);
			}
		}

		finished=true;//set flag to true, then false if any services still running
		for (ETLService service:exportJobList)//check service status
		{
			if (service.isFinished() && !service.stopRequested())//if the job's finished we either:(a) give it more work to do, or (b) let it be finished
			{
				service.setStopRequested(true);//this service is finished so we mark it as stop requested
				logger.info("Export trying list: "+jobCursor+" : "+new Date(System.currentTimeMillis()).toString());
				Boolean nextJobStarted=(getNextJobStarted(jobCursor));//start replacement worker
				
				if (nextJobStarted)
				{										
					jobCursor++;
					finished=false;//if the next job started then we assume we aren't finished
				}						
				
			}			
			
			else if (!service.isFinished() && !service.stopRequested())
			{
				finished=false;
			}			
			logger.info("Export has loaded "+service.getLoadCount()+" "+service.getListName()+" records.");
		}


	}
	
	logger.info("Export stopping: "+new Date(System.currentTimeMillis()).toString());
}


private static Boolean getNextJobStarted(Integer jobCursor) throws Exception
{
	Boolean moreJobs=true;
	final String listProperty="savelist."+jobCursor+".name";
	final String fileProperty="savelist."+jobCursor+".filename";
	final String loadMaxProperty="savelist."+jobCursor+".loadmax";
	final String mergeProperty="savelist."+jobCursor+".merge";
	final String sambaSelectProperty="session.use.samba.list.reads";

	String listName=ConfigurationUtility.getProperty(listProperty);
	String fileName=ConfigurationUtility.getProperty(fileProperty);
	String mergePropertyValue=ConfigurationUtility.getProperty(mergeProperty);
	String sambaPropertyValue=ConfigurationUtility.getProperty(sambaSelectProperty);

	Boolean isMergeActive;
	if (mergePropertyValue==null)
	{
		isMergeActive=false;
	}
	else
	{
		isMergeActive=Boolean.valueOf(mergePropertyValue);
	}	
	
	Boolean isSambaActive;
	if(sambaPropertyValue==null)
	{
		isSambaActive=false;
	}
	else
	{
		isSambaActive=Boolean.valueOf(sambaPropertyValue);
	}
	
	if (listName==null && fileName==null)
	{
		moreJobs=false;
	}

	else if (listName!=null && fileName==null)
	{
		throw new Exception ("Export job did not start because configured list: "+listProperty +" value: "+listName+" is configured but "+fileProperty+" has not been set.");
	}

	else if (fileName!=null && listName==null)
	{
		throw new Exception ("Export job did not start because configured file: "+fileProperty +" value: "+fileName+" is configured but "+listProperty+" has not been set.");
	}

	else //check if file and list exist
	{
		

		String loadMaxVal=ConfigurationUtility.getProperty(loadMaxProperty);

		loadMax=Integer.valueOf(loadMaxVal);

		if (loadMax==null)
		{
			throw new Exception("Export job did not start because "+loadMaxProperty+" has not been set.");
		}		

		UniSelectList fileSelect = em.getSelect(listName);		
		UniString testRecordId=null;

		if (fileSelect!=null)
		{
			testRecordId=fileSelect.next();//test if a record is returned
			em.closeSelect(listName);//reset cursor
		}

		if (fileSelect==null || testRecordId==null || testRecordId.getBytes().length==0)
		{
			throw new Exception ("Export job did not start because configured list: "+listProperty+" value: "+listName+" doesn't exist or has no records.");
		}

		else
		{

			try
			{
				em.getRecord(fileName, testRecordId.toString());//test if record belongs to the correct table			
			}

			catch (Exception e)
			{
				throw new Exception ("Export job could not start because configured file property: "+fileProperty+" value: "+fileName+" doesn't exist or doesn't contain a record found in list "+listName+".");
			}					

		}		

		for (Thread jobThread:threadList)
		{
			if (listName!=null && jobThread.getName()!=null && listName.equals(jobThread.getName()))
			{
				if(Boolean.TRUE.equals(jobThread.isAlive()))//stop any outstanding threads that are using the same list name
				{
					jobThread.interrupt();
				}
			}
		}
		
		
		configuredLists.put(listName, fileName);
		ETLService service = new ETLService();
		service.setListName(listName);
		service.setFileName(fileName);
		service.setLoadMax(loadMax);
		service.setIsMergeRecordsActive(isMergeActive);
		service.setIsSambaListReadActive(isSambaActive);

		exportJobList.add(service);
		Thread thread = new Thread(service);
		thread.setName(listName);
		threadList.add(thread);
		thread.start();

	}
	
	return moreJobs;

}


}

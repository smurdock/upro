package com.ihcfs.UniverseEntities.PickUtilities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeDateConverter {
	
	public static Date externalDate(String internalDate){//convert julian to Date
		
		SimpleDateFormat dateFormat=null;
		Date externalDate=null;
		
		if ("".equals(internalDate)|| internalDate==null){

		}
		
		else if (internalDate.contains("-") && !internalDate.startsWith("-")) //date contains dashes (not julian)
		{
			if((internalDate.split("-").length==3));
			{
				SimpleDateFormat format = new SimpleDateFormat ("MM-dd-yy");
				
				try
				{
					externalDate= format.parse(internalDate);
					
				}
				
				catch (Exception e)
				{
					System.out.println("Unable to convert date:"+internalDate);
				}
				
			}
		}
		
		else if (internalDate.contains("\\")){//date contains slashes (not julian)
			if(internalDate.split("\\\\").length==3)
			{
				String internalDateComponents[]=internalDate.split("\\\\");
				String month = internalDateComponents[0];
				String day   = internalDateComponents[1];
				String year  = internalDateComponents[2];
				if (2==year.length())
				{
					dateFormat= new SimpleDateFormat ("MM\\dd\\yy");
					try {
						externalDate = dateFormat.parse(internalDate);
					} catch (ParseException e) {
						System.out.println("Unable to convert date:"+internalDate);
					}
				}
				
				else if (4==year.length())
				{
					dateFormat= new SimpleDateFormat ("MM\\dd\\yyyy");
					try {
						externalDate = dateFormat.parse(internalDate);
					} catch (ParseException e) {
						System.out.println("Unable to convert date:"+internalDate);
					}
				}
				
				else
				{
					System.out.println("Unable to convert date:"+internalDate);
				}
					
			}
		}
			
		else{//julian
			externalDate= new Date((Long.valueOf(internalDate)-731l)*24l*60l*60l*1000l-64800000l);
			                                          //convert to milliseconds, then subtract 18 hours		
			if (!TimeZone.getDefault().inDaylightTime( externalDate ))
			{
				externalDate=new Date(externalDate.getTime()+3600000l);// add an hour due to no DST, java appears to subtract an hour if DST wasn't active on the day the time was stored
			}
		}
				
		return externalDate;
	}

	public static Long internalDate(Date javaDate){

		Long milliSecondsSinceEpoch = javaDate.getTime();//this is the milliseconds since the epoch date, after converting the date to GMT
		BigDecimal milliSecondsSinceEpochBig = new BigDecimal(milliSecondsSinceEpoch);

		BigDecimal daysSinceJavaEpochBig;
		if (milliSecondsSinceEpoch<0l){
			daysSinceJavaEpochBig = milliSecondsSinceEpochBig.divide(new BigDecimal(24l * 60l * 60l * 1000l), 0, RoundingMode.UP);//we always want a fraction of a day to disappear
		} else {
			daysSinceJavaEpochBig = milliSecondsSinceEpochBig.divide(new BigDecimal(24l * 60l * 60l * 1000l), 0, RoundingMode.DOWN);//we always want a fraction of a day to disappear
		}

		Long daysSincePickEpoch = daysSinceJavaEpochBig.longValue()+732l;//Pick epoch date is 732 days earlier than Java epoch date
		return daysSincePickEpoch;
		
	}

	public static Long internalTime(Date externalDate){
		Long secondsToTheHour =Long.valueOf(externalDate.getHours())*60l*60l;
		Long secondsToTheMinute = Long.valueOf(externalDate.getMinutes()*60l);
		Long secondsToTheSecond = Long.valueOf(externalDate.getSeconds());
		Long pickInternalDate = secondsToTheHour+secondsToTheMinute+secondsToTheSecond;
		return pickInternalDate;
		
	}	

}
